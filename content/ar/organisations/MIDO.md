---
name: Myan ICT for Development Organization (MIDO)
website: https://http://www.mido.ngo
logo: MIDO_logo_blue_fill.png
languages: ဗမာ, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, browsing, account, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: كل يوم من 09:00 إلى 17:00 UTC +6:30
response_time: يوم
contact_methods: email, phone, signal
email: help@mido.ngo
phone: +95(9)777788258 +95(9)777788246
whatsapp: +95(9)777788258
---

مهمّة ميدو تحفيز تبنّي تقنية المعلوماتية لدفع التقدُّم الاجتماعي و&nbsp;السياسي في ميانمار.

