---
layout: page
title: "ثمّة من سرّب بياناتي الخاصّة أ و&nbsp;نشر معلومات أ و&nbsp;ميديا عنّي دون إذني"
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Flo Pagano, Constanza Figueroa, Alex Argüelles
language: ar
summary: "Private information about me or media featuring my likeness is being circulated. It is distressing to me or could potentially be harmful"
date: 2023-05
permalink: /en/topics/doxing
parent: Home
---

# ثمّة من سرّب بياناتي الخاصّة أ و&nbsp;نشر معلومات أ و&nbsp;ميديا عنّي دون إذني

قد ينشر متحرّشون على الإنترنت بيانات تُعرِّفك شخصيًّا أ و&nbsp;تكشف جوانب من حياتك الشخصية اخترت إبقاءها في الحيّز الخاص، و&nbsp;ه و&nbsp;ما يُشار إليه بالمُسمّى doxing.

الغرض من نشر البيانات الخاصة إحراج الشخص المستهدف أ و&nbsp;التحريض ضده، و&nbsp;قد تكون لهذا تبِعات سلبية بالغة على السلامة النفسية الاجتماعية و&nbsp;الأمان الشخصي و&nbsp;العلاقات و&nbsp;العمل.

يُستخدم نشر البيانات الخاصة بغرض تخويف الشخصيات العامة و&nbsp;الصحافيين و&nbsp;المدافعين عن حقوق الإنسان و&nbsp;النشطاء، كما يشيع استخدامه ضدّ الجنسية و&nbsp;المجموعات المُهمّشة، و&nbsp;ه و&nbsp;قريب الصّلة بالأنواع الأخرى من العنف و&nbsp;التحرش السبرانيين.

الشخص الذي ينشر بياناتك قد يكون قد حصل عليها من مصادر عَدَّة، بما فيها اختراق حساباتك الخاصة و&nbsp;نبائطك، إلا أنهم قد يستخدمون معلومات أ و&nbsp;ميديا مقتطعة من سياقها ربّما تكون قد شاركتها مع أصدقائك أ و&nbsp;شركائك أ و&nbsp;معارفك أ و&nbsp;زملائك. أحيانًا يلجأ المعتدون إلى تجميع معلومات متاحة في مصادر علنية عن الشخص المُستهدف من منصات التواصل الاجتماعي أ و&nbsp;السجلات العلنية، فجمع تلك المعلومات و&nbsp;تأطيرها على نح و&nbsp;خبيث ثم نشرها يُعد أيضا نوعًا من تسريب البيانات المؤذي.

إذا تعرضت لستريب بياناتك فيمكنك اتّباع هذا التحليل لمعرفة أين وجد المتحرّش بياناتك و&nbsp;التعرّف على وسائل لتدارك الضرر، بما في ذلك إزالة البيانات من مواقع الوِب.

## Workflow

### be_supported

> التوثيق هام للغاية لتقييم الوقع و&nbsp;التعرّف على مَصْدر الهجمات و&nbsp;تطوير استجابة لتوكيد سلامتك. صيرورة التوثيق قد تجدد من الوقع السلبي للاعتداء أ و&nbsp;تزيده، و&nbsp;لتقليل عبء هذا العمل قد تحتاجين إلى مساعدة من أصدقاء قريبين أ و&nbsp;آخرين ممن تثقين فيهم لمساعدتك في توثيق الاعتداء.

هل لديك من تثقين فيه و&nbsp;تمكنه مساعدتك عند الحاجة؟

 - [نعم، يوجد من أثق فيه و&nbsp;تمكنه مساعدتي](#physical_risk)

### physical_risk

> فكّر فيمن يمكن أن يكون وراء هذا الاعتداء، و&nbsp;في قدراته، و&nbsp;درجة تصميمه على إيذائك، و&nbsp;في الجهات الأخرى التي لديها دافع لإيذائك. هذه المُحدّدات ستعينك على تقييم التهديدات القائمة و&nbsp;معرفة ما إذا كنت مثُرضة لخطر جسدي.

هل تخشى على سلامتك الجسدية أ و&nbsp;النفسية أ و&nbsp;القانونية؟

 - [نعم، أعتقد أنني معرّضة لخطر جسدي](#yes_physical_risk)
 - [أظن أنني مُعرّض لخطر قانوني](#legal_risk)
 - [لا، لكنني أريد حلّ المشكلة في الفضاء السبراني](#takedown_content)
 - [لست متأكّدة. أحتاج إلى مساعدة لتقييم الموقف](#assessment_end)

### yes_physical_risk

> إذا شعرت بأنّك قد تكوني مهددة لخطر جسدي أ و&nbsp;أن سلامتك النفسية مُهدّدة فيمكنك اللجوء إلى المصادر التالية لمساعدتك على تحديد المطلوب لتأمينك الجسدي.
>
> - [فرُنْتلَين دِفِندرز - كتاب العمل الخاص بالأمان](https://www.frontlinedefenders.org/ar/workbook-security)
> - [National Domestic Violence Hotline - Create a Safety Plan](https://www.thehotline.org/plan-for-safety/create-a-safety-plan/)
> - [Coalition against Online Violence - Physical Security Support](https://onlineviolenceresponsehub.org/physical-security-support)
> - [Cheshire Resilience Forum - How to Prepare for an Emergency](https://cheshireresilience.org.uk/how-to-prepare-for-an-emergency/)
> - [FEMA Form P-1094 Create Your Family Emergency Communication Plan](https://www.templateroller.com/group/12262/create-your-family-emergency-communication-plan.html)
> - [Reolink - How to Cleverly Secure Your Home Windows — Top 9 Easiest Security Solutions](https://reolink.com/blog/top-7-easy-diy-ways-to-secure-your-home-windows/)
> - [Reolink - How to Make Your Home Safe from Break-ins](https://reolink.com/blog/make-home-safe-from-break-ins/)

هل تحتاجين المزيد من المساعدة لتأمينك جسديًّا و&nbsp;نفسيًّا؟

 - [نعم](#physical-risk_end)
 - [لا، لكنني مًعرّضة كذلك لخطر قانوني](#legal_risk)
 - [لا، لكنني أريد حلّ المشكلة في الفضاء السبراني](#takedown_content)
 - [لست متأكّدة. أحتاج إلى مساعدة لتقييم الموقف](#assessment_end)

### legal_risk

> إذا كنت تعتزم اتخاذ إجراءات قانونية فحفظ القرائن على الهجوم الذي تعرّضت له هامٌّ للغاية، لذا من المُحبّذ اتّباع [توصيات توثيق الهجمات و&nbsp;الاعتداءات](/../../documentation).

هل تحتاج إلى مساعدة قانونية عاجلة؟

 - [نعم](#legal_end)
 - [لا، لكنني أريد حلّ المشكلة في الفضاء السبراني](#takedown_content)
 - [لا](#resolved_end)

### takedown_content

> أتريدين محاولة إزالة الوثائق و&nbsp;المعلومات المؤذية من الحيّز العام؟

 - [نعم](#documenting)
 - [لا](#resolve_end)

### documenting

> توثيق الاعتداء الذي تعرّضت له هامُّ للغاية، و&nbsp;يسهّل عمل القانونيين أ و&nbsp;التقنيين الذين يساعدونك. طالع [توصيات توثيق الهجمات و&nbsp;الاعتداءات](/../../documentation) لمعرفة كيفية جمع المعلومات حسب احتياجاتك.
>
> قد يكون من الأفضل أن تطلبي المساعدة من شخص تثقين فيه لينظر في قراءن الهجوم الذي تعرضت له، لكي لا يتجدّد كربك بمطالعتها بنفسك.

هل تحسي أنّك قادر على توثيق الهجوم على نح و&nbsp;آمن لك؟

- [نعم، أنا أقوم بتوثيق الاعتداء](#where_published)

### where_published

> يتوقّف التصرّف الممكن على الموضع الذي نُشِرَت فيه المعلومات و&nbsp;الوثائق و&nbsp;الميديا.
>
> إذا كان المحتوى قد نُشِر في منصّات التواصل الاجتماعي الموجودة في قضاءات ذات نظم قانونية تُحدِّد مسؤولية الشركات تجاه الأفراد من مستخدميها، مثل دول الاتحاد الأوربي و&nbsp;الولايات المُتحّدة، فعلى الأرجخ أن إدارة الشركة مُشغّلة الخدمة سيكون لديها دافع لمساعدتك، و&nbsp;سيطلبون منك [توثيق](#documentation) قرائن على الهجمات. تمكنك معرفة الدولة التي تقع فيها الشركة بطريق مطالعة مقالة الخدمة في ويكيبيديا.
>
> لكن أحيانًا ينشر المهاجمون المحتوى في مواقع على الوِب أصغر و&nbsp;غير معروفة، و&nbsp;يربطون إليها من منصات التواصل الاجتماعي الشهيرة، و&nbsp;أحيانًا ما تكون تلك المواقع مُستضافة في دول ليست بها ضمانات قانونية أ و&nbsp;تشغّلها مجموعات أ و&nbsp;أفراد يمارسون هذا النوع من السلوك المؤذي أ و&nbsp;يقبلون به أ و&nbsp;لا يأبهون به. في هذه الحالة قد يكون من الأصعب التواصل مع مديري تلك المواقع أ و&nbsp;حتّى معرفة من يشغّلها.
>
> لكن حتّى إذا نشر المعتدون المحتى المؤذي في مواقع غير منصات التواصل الاجتماعي الشهيرة، و&nbsp;ه و&nbsp;ما يحدث في أغلب الحالات فإنّ محاولة إزالة نسخ ذلك المحتوى من منصات التواصل الاجتماعي أ و&nbsp;الروابط إليه ستُقلِّل من عدد من يطالعونها، فتقلّ فداحة الهجوم.

أين نُشرت معلوماتك الخاصة؟

- [في منصّة تواصل اجتماعي تحت طائلة القانون](#what_info_published)
- [في منصّة أ و&nbsp;خدمة من غير المُرجّح أن تستجيب لطلبات الإزالة](#legal_advocacy)

### what_info_published

ما طبيعة ما نُشر بغير إذنك؟

- [بيانات شخصية تُعرِّفني أ و&nbsp;حسّاسة، مثل عنوان سكني أ و&nbsp;رقم تلفوني أ و&nbsp;رقم هويتي الوطنية أ و&nbsp;بيانات حسابي البنكي](#personal_info)
- [ميديا لم أوافق على نشرها، بما فيها المواد الحميمية (الصور و&nbsp;الفيديوات و&nbsp;التسجيلات الصوتية و&nbsp;النصوص)](#media)
- [كنية أ و&nbsp;اسم مستخدم أستخدمه تمّ كشف ارتباطه بهويتي الماديّة](#linked_identities)

### linked_identities

> إذا ما كشف المعتدي اسمك القانوني أ و&nbsp;هويّتك أ و&nbsp;اسما حركيًا أ و&nbsp;كنية أ و&nbsp;اسم مستخدم تستخدمينه في التعبير عن نفسك أ و&nbsp;نشر آرائك أ و&nbsp;التنظيم أ و&nbsp;الانخراط في الناشطية، ففكّري في غلق الحساب المرتبط بتلك الهويّة لتقليل الضرر.

هل تحتجاين إلى مواصلة استعمال هذه الهوية\الشخصية لتحقيق أهدافك أ و&nbsp;إنجاز عملك؟

- [نعم علي مواصلة استعمالها](#defamation_flow_end)
- [لا، يمكنني غلق تلك الحسابات](#close)

### close

> قبل غلق الحسابات المرتبطة بالهويّة المتأثّرة بالهجوم أ و&nbsp;إنهاء الهوية، فكّر في مخاطر فعل ذلك، فقد يتعذّر عليك الوصول إلى خدمات أ و&nbsp;بيانات تحتاجها، أ و&nbsp;قد تتأثر سمعتك في وسط ما، أ و&nbsp;ينقطع اتّصالك بمعارف و&nbsp;زملاء سبرانيين، إلخ.

- [قفلت الحسابات المتأثّرة](#resolved_end)
- [قرّرت عدم قفل الحسابات في الوقت الحالي](#resolved_end)
- [أحتاج إلى مساعدة لتحديد ما ينبغي فعله](#harassment_end)

### personal_info

أين نُشِرَت بياناتك الشخصية؟

- [في منصة تواصل اجتماعي](#doxing-sn)
- [في موقع وِب](#doxing-web)

### doxing-sn

> إذا كانت بياناتك الخاصة قد نُشِرَت في منصة تواصل اجتماعي فيمكنك الإبلاغ عن انتهاك قواعد الاستخدام باتّباع إجراء الإبلاغ الذي تُقدّمه الخدمة.
>
> ***ملاحظة:*** * دومًا [وثِّقي](/../../documentation) الانتهاك قبل اتخذا إجراءات مثل حذف الرسائل أ و&nbsp;المحاورات أ و&nbsp;حظر المستخدمين، و&nbsp;إذا كنت تعتزم اتّخاذ إجراء قانوني فتُستحسن مطالعة [متطلبات التوثيق القانوني](/../../documentation#legal).*
>
> توجد إرشادات للإبلاغ عن مخالفات شروط الاستخدام في المنصات الاجتماعية الأشهر:
>
> - [گوگل](https://cybercivilrights.org/google-2/)
> - [فيسبوك](https://www.cybercivilrights.org/facebook)
> - [توِتر](https://www.cybercivilrights.org/twitter)
> - [تَمْبْلَر](https://www.cybercivilrights.org/tumblr)
> - [إنستَگرام](https://www.cybercivilrights.org/instagram)
>
> مع ملاحظة أنّه قد يمرّ بعض الوقت قبل أن تتلقى ردًّا على بلاغك.

هل أزيل المحتوى المُبلغ عنه؟

 - [نعم](#one-more-persons)
 - [لا](#harassment_end)

### doxing-web

يمكنك الإبلاغ عن الموقع لدى مُقدِّم خدمة الاستضافة أ و&nbsp;مُسجَل اسم النطاق، طالبًا إزالته.

> ***ملاحظة:*** * دومًا [وثِّقي](/../../documentation) الانتهاك قبل اتخاذ إجراءات مثل حذف الرسائل أ و&nbsp;المحاورات أ و&nbsp;حظر المستخدمين، و&nbsp;إذا كنت تعتزم اتّخاذ إجراء قانوني فتُستحسن مطالعة [متطلبات التوثيق القانوني](/../../documentation#legal).*
>
> لإرسال طلب إزالة يتوجّب عليك الحصول على معلومات عن موقع الوِب المنشورة في بياناتك الخاصة:
>
> - Go to [Network Tools' NSLookup service](https://network-tools.com/nslookup/) and find out the IP address (or addresses) of the fake website by entering its URL in the search form.
> - Write down the IP address or addresses.
> - Go to [Domain Tools' Whois Lookup service](https://whois.domaintools.com/) and search both for the domain and the IP address/es of the fake website.
> - Record the name and abuse email address of the hosting provider and domain service. If included in the results of your search, also record the name of the website owner.
> - Write to the hosting provider and domain registrar of the fake website to request its takedown. In your message, include information on the IP address, URL, and owner of the impersonating website, as well as the reasons why it is abusive.
>
> مع ملاحظة أنّه قد يمرّ بعض الوقت قبل أن تتلقى ردًّا على بلاغك.

هل أزيل المحتوى المُبلغ عنه؟

- [نعم](#resolved_end)
- [لا، المنصة لم تستجب لطلبي](#platform_help_end)
- [أريد معرفة كيفية تقديم طلب إزالة](#platform_help_end)

### media
> إذا كنت قد صنعت الميديا بنفسك، فعادة ما تؤول حقوق طبعها لك. في بعض القضاءات، تكون للأفراد بعض الحقوق في الميديا التي يظهرون فيها، و&nbsp;تُعرف بحقوق الإشهار أ و&nbsp;حقوق الشخصية.

هل تملك حقوق طبع الميديا؟

- [نعم](#intellectual_property)
- [لا](#nude)

### intellectual_property

> يمكنك اللجوء إلى تنظيمات حقوق الطبع، مثل قانون [Digital Millennium Copyright Act (DMCA)](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act) الأمريكي، لحذف المحتوى. أغلب منصات التواصل الاجتماعي تنشر استمارات للإبلاغ عن انتهاكات حقوق الطبع و&nbsp;قد تتجاوب مع هذا النوع من البلاغات أكثر من غيره بسبب التّبعات القانونية الفادحة عليهم.
>
> **ملاحظة:*** *احرصي دومًا على [التوثيق](/../../documentation) قبل طلب الحذف، و&nbsp;إذا كنت تنتوين اللجوء إلى القضاء فتُستحسن مطالعة القسم المتعلّق [بالتوثيق لأجل التقاضي](/../../documentation#legal).*
>
> يمكنك باتّباع الإجراءات الموصوفة في الروابط التالية لطلب إزالة محتوى ينتهك حقوق الطبع:
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [فيسبوك](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [إنستَگرام](https://help.instagram.com/contact/552695131608132)
> - [تِكْ‌تُك](https://www.tiktok.com/legal/report/Copyright)
> - [توِتَر](https://help.twitter.com/ar/forms/ipi)
> - [يوتيوب](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
>
> مع ملاحظة أنّ تلّقي ردٍّ على طلبك قد يستغرق بعض الوقت.

هل أُزيل المحتوى المُبلغ عنه؟

 - [نعم](#resolved_end)
 - [لا، و&nbsp;أريد مساعدة قانونية](#legal_end)
 - [لا، و&nbsp;أريد مساعدة للتواصل مع إدارة المنصّة](#harassment_end)

### nude

> الميديا التي تُصوِّر أجسامًا عارية أ و&nbsp;أجزاء مفصّلة منها تكون أحيانا مشمولة بسياسات خاصّة لمنصات التواصل الاجتماعي.

هل تظهر في الميديا المستخدمة في الحملة أجسام عارية؟

- [نعم](#intimate_media)
- [لا](#nonconsensual_media)

### intimate_media

> لمعرفة كيفية إبلاغ منصّات التواصل الاجتماعي عن انتهاك تنظيمات الخصوصيّة أ و&nbsp;شاركة ميديا حميمية بغير إذن، طالعي المصادر التالية:
>
> - [Stop NCII](https://stopncii.org/) (Facebook, Instagram, TikTok and Bumble - worldwide)
> - [Revenge Porn Helpline - Help for Victims Outside the UK](https://revengepornhelpline.org.uk/how-can-we-help/if-we-can-t-help-who-can/help-for-victims-outside-the-uk/)
>
> **ملاحظة:*** *احرص دومًا على [التوثيق](/../../documentation) قبل طلب الحذف، و&nbsp;إذا كنت تنتوين اللجوء إلى القضاء فتُستحسن مطالعة القسم المتعلّق [بالتوثيق لأجل التقاضي](/../../documentation#legal).*
>
> مع ملاحظة أنّ تلّقي ردٍّ على طلبك قد يستغرق بعض الوقت.

هل أزيل المحتوى المؤذي؟

- [نعم](#resolved_end)
- [لا، فالمنصة لم تردّ أ و&nbsp;لم تتجاوب](#platform_help_end)
- [لم أجد معلومات عن كيفية عمل ذلك في المنصّة المعنية](#platform_help_end)

### nonconsensual_media

أين نُشِرَت الميديا المؤذية؟

- [في منصة للتواصل الاجتماعي](#NCII-sn)
- [على موقع وِب](#NCII-web)

### NCII-sn

> إذا كانت الميديا قد نُشِرَت في منصّة للتواصل الاجتماعي فيمكنك الإبلاغ عن انتهاك القواعد الحاكمة للخدمة باتّباع إجراءات الإبلاغ الموصوفة للمستخدمين من طرف مديري منصة التواصل الاجتماعي.
>
> **ملاحظة:*** *احرصي دومًا على [التوثيق](/../../documentation) قبل طلب الحذف، و&nbsp;إذا كنت تنتوين اللجوء إلى القضاء فتُستحسن مطالعة القسم المتعلّق [بالتوثيق لأجل التقاضي](/../../documentation#legal).*
>
> تجدون فيما يلي إرشادات لكيفية الإبلاغ عن مخالفات قواعد الاستخدام لمنصات التواصل الاجتماعي الأشهر (بالإنگليزية):
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> مع ملاحظة أنّ تلّقي ردٍّ على طلبك قد يستغرق بعض الوقت.

هل أزيل المحتوى المؤذي؟

- [نعم](#resolved_end)
- [لا، فالمنصة لم تردّ أ و&nbsp;لم تتجاوب](#platform_help_end)
- [لم أجد معلومات عن كيفية عمل ذلك في المنصّة المعنية](#platform_help_end)

### NCII-web

> اتّبعي الإرشادات المبيّنة في "[Without My Consent - Take Down](https://withoutmyconsent.org/resources/take-down)" لإزالة محتوى من موقع وِب.
>
> **ملاحظة:*** *احرصي دومًا على [التوثيق](/../../documentation) قبل طلب الحذف، و&nbsp;إذا كنت تنتوين اللجوء إلى القضاء فتُستحسن مطالعة القسم المتعلّق [بالتوثيق لأجل التقاضي](/../../documentation#legal).*
>
> مع ملاحظة أنّ تلّقي ردٍّ على طلبك قد يستغرق بعض الوقت.

هل أزيل المحتوى المؤذي؟

- [نعم](#resolved_end)
- [لا، فالمنصة لم تردّ أ و&nbsp;لم تتجاوب](#platform_help_end)
- [أحتاج مساعدة في طلب الإزالة](#platform_help_end)

### legal_advocacy

> في بعض الأحيان لا تكون لدى منصّة التواصل الاجتماعي التي يجري عبرها تسريب المعلومات الشخصية أ و&nbsp;نشر الميديا الشخصية سياسات أ و&nbsp;صيرورة إدارية ناضجة بما يكفي لمعالجة بلاغات تسريب المعلومات الشخصية أ و&nbsp;التشهير.
>
> و&nbsp;أحيانا يُنشَر المحتوى المؤذي عبر منصّات أ و&nbsp;مواقع وِب يديرها أشخاص أ و&nbsp;مجموعات تمارس ذلك النوع من الانتهاكات أ و&nbsp;تقبله أ و&nbsp;لا تعبأ به.
>
> كذلك توجد مواضع في الفضاء السبراني يصعب تطبيق القوانين و&nbsp;التنظيمات المعتادة عليها لأنّها منشأة بغرض حفظ مجهولية مستخدميها، و&nbsp;هي ما تُغرَف في مجملها باسم "الوِب المُظلِمة". و&nbsp;بالرغم من احتمالات استعمالها في الإيذاء فإنّه توجد فوائد من إنشاء فضاءات كهذه، لأهميّتها في التمكين من ممارسة الحقّ في التعبير، و&nbsp;ممارسة الحقّ في استقاء و&nbsp;تداول المعلومات بلا منع و&nbsp;لا رقابة، و&nbsp;عدل كفة القوّة بين الأفراد و&nbsp;الحكومات عمومًا و&nbsp;الشمولية منها بالذات.
>
> في مثل هذه الحالات التي لا يكون فيها اتّخاذ إجراءات قانونيّة ممكنا فقد تريدون اللجوء إلى حملات المناصرة، أي بتسليط الضوء على حالتك و&nbsp;إثارة نقاش مجتمعي عام بشأنها، و&nbsp;هم ما قد يؤدي كذلك إلى ظهور حالات مشابهة. توجد منظّمات حقوقيّة و&nbsp;مجموعات تمكنها مساعدتك في هذا، و&nbsp;في ضبط توقّعاتك من النتائج و&nbsp;في تقدير للتَبِعات المحتملة.
>
> اللجوء إلى الدعم القانوني قد يكون وسياتك الأخيرة في حال انعدام جدوى من التواصل مع منصة التواصل الاجتماعي المعنية أ و&nbsp;تعذُّره.
>
> المسار القانوني عادة ما يستغرق وقتًا و&nbsp;يُكلَّف مالا و&nbsp;قد يتطلّب إشهار الواقعة. كما أنّ نجاح التقاضي يعتمد نجاحه على عوامل عديدة منها [التوثيق](#documentation#legal) الجيّد المقبول للمحكمة و&nbsp;الإطار القانوني الحاكم للقضيّة. كما أنّ تحديد الجُناة أ و&nbsp;إثبات مسؤوليّتهم قد يكون صعبًا، كما قد يَصعُبُ أيضًا تحديد نطاق الولاية القضائية التي ينبغي نظر القضية فيها.

ما الذي تريدين فعله؟

- [أحتاج مساعدة في تخطيط حملة مناصرة](#advocacy_end)
- [أحتاج دعمًا قانونيًا](legal_end)
- [لدي دعم من مجموعتي](#resolved_end)

### advocacy_end

> إذا أردت مجابهة المعلومات المنتشرة بغير إذنك فنحن نحبّذ مطالعة الإرشادات الواردة في [تحليل عُدَّة الإسعاف الأوّلي الرقمي بشأن حملات التشويه](../../../defamation).
>
> و&nbsp;يمكنك التواصل مع المنظّمات التالية التي قد تمكنها مساعدتك في تنظيم حملة مناصرة:

:[](organisations?services=advocacy)

### legal_end

> إذا احتجت إلى الدعم القانوني فالمنظمات التالية قد تمكنها تقديمه.
>
> تذكّري دومًا أن توثيق الانتهاك و&nbsp;الهجوم عليك ضروري في المسارات القانونية، لذا نحثّك على مطالعة [توصياتنا في عُدَّة الإسعاف الأوّلي الرقمية المتعلّقة بتوثيق الانتهاكات](/../../documentation).

:[](organisations?services=legal)

### physical-risk_end

> إذا كنت تستشعر تهديدًا جسديًّا و&nbsp;تحتاج مساعدة عاجلة فاتّصل بإحدى المنظمات التالية لطلب المساعدة:

:[](organisations?services=physical_security)


### assessment_end

> إذا كنت تحتاج إلى مساعدة في تقدير الخطر الذي تواجهه من جرَّاء تسريب بياناتك الشخصية أ و&nbsp;ميديا بدون إذنك فتواصل مع إحدى المنظّمات التالية:

:[](organisations?services=harassment&services=assessment)

### resolved_end

عسى أن تكون عدَّة الإسعاف اﻷولي الرقمي قد أفادتكم، و&nbsp;نحبُّ معرفة رأيكم [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### defamation_flow_end

> إذا أردت مجابهة المعلومات المنتشرة بغير إذنك فنحن نحبّذ مطالعة الإرشادات الواردة في [تحليل عُدَّة الإسعاف الأوّلي الرقمي بشأن حملات التشويه](../../../defamation).
>
> و&nbsp;إذا احتجت مساعدة متخصّصة فيمكنك التواصل مع إحدى المنظّمات التالية:

:[](organisations?services=advocacy&services=legal)

### platform_help_end

> القائمة التالية تضمّ منظّمات يمكنها مساعدتك في تقديم البلاغات إلى المنصات و&nbsp;الخدمات.

:[](organisations?services=harassment&services=legal)

### harassment_end

> إذا احتجت مساعدة فتواصل مع إحدى المنظمات التالية.

:[](organisations?services=harassment&services=triage)


### final_tips

- ابحثي باسمك على الإنترنت. الكشف عن البيانات المتاحة عنك في مصادر علنية يساعدك على معرفة ما قد يستخدمه آخرون ضدّك. تمكنك معرفة المزيد عن كيفية تقفّي أثرك السبراني في [Access Now Helpline's Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- إذا كان التسريب يتضمّن معلومات أ و&nbsp;وثائق لا يعرفها غيرك أ و&nbsp;قلّة من الموثوق فيهم فينبغي عليك التفكير في كيفية حدوث التسريب. راجع ممارسات أمانك الشخصي لزيادة أمان حساباتك و&nbsp;نبائطك.
- توخَّ الحرص في أعقاب وقوع التسريب، حتّى إذا كنت قد نجحت في تداركه تحسّبا لظهورها أ و&nbsp;بعضها مُجدّدًا. إلى جانب البحث باسمك و&nbsp;بالكُنى التي تستخدمها على الإنترنت، يمكنك كذلك ضبط محرّك بحث مثل گوگل ليُنبّهك برسالة عند تَجدُّد ظهور اسمك في صفحات أ و&nbsp;مواقع.

#### resources

- [Crash Override Network - So You’ve Been Doxed: A Guide to Best Practices](https://crashoverridenetwork.tumblr.com/post/114270394687/so-youve-been-doxed-a-guide-to-best-practices)
- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Ken Gagne, Doxxing defense: Remove your personal info from data brokers](https://www.computerworld.com/article/2849263/doxxing-defense-remove-your-personal-info-from-data-brokers.html)
- [Totem Project - Keep it private](https://learn.totem-project.org/courses/course-v1:IWMF+IWMF_KP_EN+001/about)
- [Totem Project - How to protect your identity online](https://learn.totem-project.org/courses/course-v1:Totem+TP_IO_EN+001/about)
- [Coalition against Online Violence - I've been doxxed](https://onlineviolenceresponsehub.org/for-journalists#doxxed)
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/)
