---
layout: page
title: البريد الإلكتروني
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/email.md
parent: /ar/
published: true
---

فحوى رسالتك و&nbsp;واقعة تواصلك مع المنظَّمة كلتاهما قد تكون معلومة للحكومة و&nbsp;أجهزة إنفاذ القانون.
