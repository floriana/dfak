---
layout: page
title: Formulario de Contacto
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/contact-form.md
parent: /es/
published: true
---

Un formulario de contacto probablemente preservará la privacidad de
tu mensaje a la organización receptora, de modo que solo tú y la
organización receptora puedan leerlo. Este será el caso solo si el sitio
web que aloja el formulario de contacto implementa las medidas de
seguridad adecuadas, como el cifrado [TLS/SSL](https://ssd.eff.org/es/glossary/secure-sockets-layer-ssl), entre otros, como es el caso de las organizaciones pertenecientes a CiviCERT.

Sin embargo, el hecho de que hayas visitado el sitio web de la
organización donde está alojado el formulario de contacto será
probablemente un hecho conocido por gobiernos, agencias de aplicación de
la ley u otras instituciones con acceso a la infraestructura de
vigilancia local, regional o global. El hecho de que hayas visitado el
sitio web de la organización indicaría que es posible que te hayas
puesto en contacto con la organización.

Si deseas mantener en privado el haber visitado el sitio web de la organización (y que potencialmente te comunicaste con ella), entonces es preferible acceder a su sitio web a través del [Navegador Tor](https://www.torproject.org/es/) o un VPN o proxy de confianza. Antes de hacerlo, considera el contexto legal donde resides y si necesitas ofuscar el uso del Navegador Tor [configurándolo](https://tb-manual.torproject.org/es/running-tor-browser/) con un [pluggable transport](https://tb-manual.torproject.org/es/circumvention/). Si estás considerando usar una VPN o un proxy, [investiga dónde se encuentra ese servidor o proxy VPN (en inglés)](https://protonvpn.com/blog/vpn-servers-high-risk-countries/) y [su confiabilidad como entidad VPN](https://ssd.eff.org/es/module/escogiendo-el-vpn-apropiado-para-usted).
