---
layout: page
title: WhatsApp
author: mfc
language: es
summary: método de contacto
date: 2018-09
permalink: /es/contact-methods/whatsapp.md
parent: /es/
published: true
---

El uso de WhatsApp asegurará que tu conversación con el destinatario esté protegida de modo que solo tú y el destinatario podáis leer las comunicaciones, sin embargo, el hecho de que te hayas comunicado con la persona destinataria puede ser un dato accesible a los gobiernos u organismos de seguridad.

Recursos: [Cómo Utilizar Whatsapp en Android](https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-whatsapp-en-android) [Cómo Utilizar Whatsapp en iOS](https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-whatsapp-en-ios).
