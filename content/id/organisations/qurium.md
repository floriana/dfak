---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: jurnalis, media, pembela HAM, aktivis, lgbti, ormas
hours: Senin-Minggu, 8AM-18PM CET
response_time: 4 jam
contact_methods: surel, pgp, form_web
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation adalah Penyedia Solusi Keamanan untuk media independen, organisasi HAM, jurnalis investigasi dan aktivis. Qurium menyediakan portofolio solusi profesional, khusus, dan aman dengan bantuan personal untuk organisasi dan individu berisiko, yang meliputi:

- *Hosting* Aman dengan mitigasi DDoS dari situs web berisiko
- Bantuan Respons Cepat untuk organisasi dan individu di bawah ancaman langsung
- Audit keamanan layanan web dan aplikasi seluler
- Pengelakan situs web yang diblokir Internet
- Investigasi forensik terhadap serangan digital, aplikasi penipuan, *malware* yang ditargetkan dan disinformasi
