---
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: 24/7, GMT+4
response_time: 3 саат
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

Media Diversity Institute - Armenia салттуу медианы жана социалдык медианы бекемдөөгө, адам укуктарын коргоо үчүн жаңы технологияларды колдонууга жана демократиялык жарандык коомду курууга багытталган коммерциялык эмес бейөкмөт уюм. Уюм сөз эркиндиги чектелгендерге сөз эркиндигине жетүүгө аракем кылып, MDI социалдык ар түрдүүлүккө чоң басым жасайт.

MDI Armenia Лондондун Media Diversity Institute менен өнөктөштүктө иштейт, бирок ошол эле учурда көз карандысыз уюм болуп саналат. 
