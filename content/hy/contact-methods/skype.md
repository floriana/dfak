---
layout: page
title: Skype
author: mfc
language: hy
summary: Կապ հաստատելու միջոցներ
date: 2018-09
permalink: /hy/contact-methods/skype.md
parent: /hy/
published: true
---

Ձեր նամակի պարունակությունը, ինչպես նաև նամակ ուղարկելու փաստը, կարող է հասանելի լինել իշխանությունների և օրենսդիր մարմինների համար։
