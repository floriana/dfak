---
name: Myanmar ICT for Development Organization (MIDO)
website: https://http://www.mido.ngo/
logo: MIDO_logo_blue_fill.png
languages: ဗမာ, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, browsing, account, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: "7 วันต่อสัปดาห์ ตั้งแต่ 9:00 น. – 17:00 น. โซนเวลา UTC +6:30"
response_time: 1 day
contact_methods: email, phone, signal
email: help@mido.ngo
phone: +95(9)777788258 +95(9)777788246
whatsapp: +95(9)777788258
---

MIDO มีภารกิจในการทำหน้าที่เป็นตัวเร่งปฏิกิริยาให้เกิดการนำเทคโนโลยีมาปรับใช้ในการขับเคลื่อนความก้าวหน้าทางสังคมและการเมืองในประเทศเมียนมาร์
