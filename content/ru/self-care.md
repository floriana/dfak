---
layout: sidebar.pug
title: "Забота о себе и о своей команде"
author: FP
language: ru
summary: "Онлайновые домогательства, угрозы и прочие виды цифровых атак могут вызывать очень сильные эмоции и разные, глубоко личные состояния. Вы можете ощущать вину, стыд, беспокойство, раздражение, смущение, беспомощность. Вы даже можете опасаться за своё психологическое и физическое благополучие."
date: 2023-04
permalink: /ru/self-care/
parent: Home
sidebar: >
  <h3>Подробнее о том, как защитить себя и команду от чрезмерных переживаний:</h3>

 <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Self-Care for Activists: Sustaining Your Most Valuable Resource</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Caring for yourself so you can keep defending human rights</a></li>
     <li><a href="https://www.frontlinedefenders.org/sites/default/files/resilience_workbook_ru.pdf">Пособие "Как сохранить устойчивость в неустойчивом мире"</a></li>
	<li><a href="https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management">Resources for Well-being & Stress Management</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Self-Care for People Experiencing Harassment</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Cyberwomen self-care training module</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Wellness and Community</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Twenty ways to help someone who's being bullied online</a></li>
    <li><a href="https://www.hrresilience.org/">Human Rights Resilience Project</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">Totem-Project Online Learning Course: Taking care of your mental health (требуется регистрация)</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_PAP_EN+001/about">Totem-Project Online Learning Course: Psychological First Aid (требуется регистрация)</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Individual Responses to Threat</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Team and Peer Responses to Threat</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Communicating ain Teams and Organisations</a></li>
    </ul>
---

# Чувствуете перегрузку?

Онлайновые домогательства, угрозы и прочие виды цифровых атак могут вызывать чрезмерные переживания и разные, глубоко личных эмоциональные состояния. Вы можете ощущать вину, стыд, беспокойство, раздражение, смущение, беспомощность. Можете опасаться за своё психологическое и физическое благополучие.

В таких ситуациях нет "правильных" чувств. У разных людей разные степени уязвимости. Мы по-разному оцениваем значение собственных персональных данных. Любые эмоции имеют право на существование. Не стоит беспокоиться насчёт того, "правильные" ваши эмоции или нет.

Начнём с такого утверждения: в том, что с вами произошло, нет вашей вины. Вы не должны винить себя. Возможно, лучше обратиться к человеку, которому вы доверяете, кто мог бы поддержать вас в экстренной ситуации.

Чтобы разобраться с онлайновой атакой, нужно собрать информацию о том, что произошло, но необязательно делать это самостоятельно. Если есть человек, которому вы доверяете, можете попросить его (или её) поддержать вас, пока вы читаете советы на этом сайте, или предоставить ему/ей доступ к вашим устройствам и аккаунтам для сбора информации.

Если вы или ваша команда нуждаетесь в эмоциональной поддержке при разбирательстве с инцидентом цифровой безопасности (или вы анализируете, что произошло), можно обратиться в [Community Mental Health Program от Team CommUNITY](https://www.communityhealth.team/). Этот проект предлагает помощь психолога в разных форматах, на разных языках и с учётом разных обстоятельств, как в срочных случаях, так и в виде текущей поддержки.