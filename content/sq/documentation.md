---
layout: sidebar.pug
title: "Dokumentimi i sulmeve digjitale"
author: Constanza Figueroa, Patricia Musomba, Candy Rodríguez, Gus Andrews, Alexandra Hache, Nina
language: sq
summary: "Këshilla për dokumentimin e llojeve të ndryshme të urgjencave digjitale."
date: 2023-05
permalink: /sq/documentation/
parent: Home
sidebar: >
  <h3>Lexoni më shumë se si të dokumentoni urgjencat digjitale:</h3>
  <ul>
    <li><a href="https://acoso.online/barbados/report-to-law-enforcement/#general-tips">Këshilla të përgjithshme, raportoni një rast dhe si të mbani evidencën nga Acoso.online</a></li>
    <li><a href="https://www.techsafety.org/documentationtips">Rrjeti Kombëtar për t'i Dhënë Fund Dhunës në Familje:</a> Këshilla për dokumentim për të mbijetuarit e abuzimit dhe ndjekjes përmes teknologjisë</li>
    <li><a href="https://chayn.gitbook.io/how-to-build-a-domestic-abuse-case-without-a-lawye/english">Chayn:</a> Si të ndërtoni një rast abuzimi në familje pa një avokat</li>
      </ul>
---

# Dokumentimi i sulmeve digjitale

Dokumentimi i sulmeve digjitale mund të shërbejë për një sërë qëllimesh. Regjistrimi i asaj që ndodh në një sulm mund t'ju ndihmojë:

* të kuptoni më mirë situatën dhe të merrni masa për të mbrojtur veten tuaj.
* të jepni dëshmi që do t'ju nevojiten ndihmësve teknikë ose ligjorë për t'ju mbështetur.
* të kuptoni më mirë modelin e sulmeve dhe të identifikoni kërcënimet shtesë.
* të mbështetni paqen tuaj mendore dhe të kuptoni se si sulmet ju ndikojnë emocionalisht.

Pavarësisht nëse po dokumentoni për të kuptuar gjërat më mirë, ose për të kërkuar mbështetje ligjore ose teknike, një regjistrim i strukturuar i sulmeve mund t'ju ndihmojë të kuptoni më mirë:

* Shkallën dhe shtrirjen e sulmit: nëse është një sulm që është kryer vetëm një herë apo bëhet fjalë për një model i përsëritur, që ju synon juve ose një grup më të madh njerëzish.
* Zakonet e sulmuesit: nëse janë të izoluar ose pjesë e një grupi të organizuar, nëse përdorin informacionin e disponueshëm në internet ose nëse aksesojnë informacionin tuaj personal privat, taktikat dhe teknologjitë që mund të përdorin, etj.
* Nëse përgjigjet që planifikoni të zbatoni do të pakësojnë kërcënimet fizike ose në internet ndaj jush, ose do t'i përkeqësojnë ato.

Ndërsa filloni të grumbulloni informacione, merrni parasysh gjërat në vijim:

* A po dokumentoni kryesisht për paqen tuaj mendore dhe/ose për të kuptuar se çfarë po ndodh?
* A dëshironi të kërkoni ndihmë teknike për të ndaluar sulmet? Ju lutemi, merrni parasysh të vizitoni [seksionin e mbështetjes së Veglërisë së Ndihmës së Parë Digjitale](../support/) për të parë se cilat organizata të CiviCERT mund t'ju ndihmojnë.
* A dëshironi po ashtu të ngritni një rast ligjor?

Në varësi të qëllimeve tuaja, mund t'ju duhet të dokumentoni sulmet në mënyra paksa të ndryshme. Nëse keni nevojë për prova për një rast ligjor, do t'ju duhet të mblidhni lloje specifike të provave që janë ligjërisht të pranueshme në gjykatë, si p.sh. numrat e telefonit dhe regjistrimet digjitale të kohës së ndodhjes së ngjarjeve të caktuara. Asistentët teknikë do të duhet të shohin prova shtesë, si p.sh. adresat e faqeve të internetit (URL-të), emrat e përdoruesve dhe pamjet e ekranit. Shihni seksionet më poshtë për më shumë detaje mbi informacionin që duhet të siguroni për secilën nga këto shtigje të mundshme që mund t’i ndiqni.

## Mbroni mirëqenien tuaj mendore dhe emocionale gjatë dokumentimit

Ballafaqimi me sulmet digjitale është stresues dhe mirëqenia juaj fizike dhe emocionale duhet të jetë prioriteti juaj i parë, pavarësisht se çfarë lloj dokumentacioni dëshironi të ndiqni. Dokumentacioni nuk duhet të shtojë stresin tuaj. Merrni gjërat në vijim kur filloni të dokumentoni një sulm:

* Ka shumë mënyra për të dokumentuar dhe regjistruar. Zgjidhni ato me të cilat jeni të kënaqur dhe që i mbështesin qëllimet tuaja për dokumentacion.
* Dokumentimi i sulmeve digjitale mund të jetë sfidues në aspekt emocional dhe mund të shkaktojë kujtime traumatike. Nëse ndiheni të mbingarkuar, merrni parasysh mundësinë për t’ia deleguar dokumentimin një kolegu, miku, personi tjetër të besuar ose grup monitorues, i cili mund të mbajë të përditësuar dokumentacionin e sulmeve pa u ritraumatizuar.
* Nëse sulmi është bërë ndaj një grupi, merrni parasysh mundësinë për të bërë rotacion të përgjegjësisë së dokumentimit midis njerëzve të ndryshëm.
* Pëlqimi është thelbësor. Dokumentimi duhet të vazhdojë për aq kohë sa personi ose grupi i përfshirë pajtohet.
* [Këtu janë disa këshilla të tjera se si t'i bëni miqtë ose familjen tuaj t'ju ndihmojë.](https://onlineharassmentfieldmanual.pen.org/guidelines-for-talking-to-friends-and-loved-ones/)

Dokumentimi i sulmeve digjitale ose i dhunës me bazë gjinore në internet nënkupton mbledhjen e informacionit mbi atë që ju ose kolektivi juaj po përballeni. Ky dokumentacion nuk duhet vetëm të jetë racional dhe teknik. Mund t'ju ndihmojë gjithashtu të përpunoni dhunën duke regjistruar se si ndiheni për çdo sulm në tekst, fotografi, audio, video, apo edhe në shprehje artistike. Ne ju rekomandojmë ta bëni këtë offline dhe të ndërmerrni hapat e përshkruar më poshtë për të mbrojtur privatësinë tuaj gjatë përpunimit të këtyre ndjenjave.

## Bëhuni gati për të dokumentuar një sulm

Pasi të keni siguruar mirëqenien tuaj, por përpara se të dokumentoni provat individuale, **krijimi i një "harte" të të gjithë informacionit përkatës** rreth sulmit mund t'ju udhëzojë në identifikimin e të gjitha provave që mund të dëshironi të mblidhni.

Pjesa më e rëndësishme e dokumentimit është **mbajtja e një regjistri (ose “log”-u) të organizuar**. Për dokumentacionin ligjor ose teknik, një regjistër që përmban vetëm pjesë kyçe të të dhënave është i rëndësishëm për të ndihmuar në demonstrimin e modeleve të qarta të sulmeve. Kur dokumentoni për paqen tuaj mendore, një dokument i thjeshtë tekstual me më pak strukturë mund të jetë i mjaftueshëm për ju. Një regjistër më i strukturuar mund t'ju ndihmojë gjithashtu të mendoni për modelet e sulmeve me të cilat përballeni. Regjistri juaj digjital mund të jetë një dokument tekstual ose një tabelë (speradsheet). Ju gjithashtu mund të mbani një ditar në letër. Zgjedhja varet nga ju.

**Të gjitha ndërveprimet digjitale lënë gjurmë (meta të dhëna - metadata) që i përshkruajnë ato**: Koha, kohëzgjatja, adresat, llogaritë e dërgimit dhe marrjes, etj. Këto gjurmë ruhen në regjistra nga pajisjet tuaja, nga telefonat dhe kompanitë e mediave sociale, nga ofruesit e internetit dhe nga të tjerët. Analizimi i tyre mund t'u japë ekspertëve ligjorë dhe teknikë informacion jetik se kush qëndron pas një sulmi. Për shembull, një regjistër i numrit/ave të telefonit të përdorur në 20 telefonata në një periudhë të caktuar kohore mund të mbështesë një raport ngacmimi.

Mbajtja e një regjistri të organizuar e të strukturuar të këtyre të dhënave do t'ju ndihmojë të demonstroni **modelet në sulme** në mënyra që mund të mbështesin një çështje ligjore, ose të ndihmoni një ekspert të sigurisë digjitale të bllokojë sulmet dhe të mbrojë pajisjet tuaja.

Informacioni i mëposhtëm do të jetë i dobishëm në shumicën e rasteve, por secili rast mund të ndryshojë. Ju mund të kopjoni dhe ngjitni fushat më poshtë në krye të një tabele (spreadsheet) ose dokumenti për të strukturuar raportet tuaja për çdo sulm, duke shtuar më shumë fusha sipas nevojës.

| Data | Koha | Adresat e emailit ose numrat e telefonit të përdorur nga sulmuesi | Lidhje të lidhura me sulmin | Emrat (duke përfshirë emrat e llogarive) të përdorura nga sulmuesi | Llojet e sulmeve digjitale, ngacmimet, dezinformatat, etj | Teknikat e përdorura nga sulmuesi |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |

Këtu janë disa shembuj të modeleve të tjera të regjistrave nga komuniteti ynë që mund të përdorni:

* [Modeli i regjistrit të incidenteve nga Linja telefonike e sigurisë digjitale të Access Now ](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/incident_log_template.md)
* [Modeli i dhunës me bazë gjinore nga Acoso.online](https://acoso.online/site2022/wp-content/uploads/2019/01/reactingNCP.pdf)

Përveç një skedari të regjistrit, **ruani gjithçka që lidhet me ngjarjen ose incidentin** në një ose më tepër dosje (folder) — duke përfshirë dokumentin tuaj të regjistrit dhe çdo dëshmi digjitale shtesë që keni eksportuar, shkarkuar ose kapur në një pamje ekrani. Edhe provat që nuk kanë peshë ligjore mund të jenë të dobishme për të treguar përmasat e një sulmi dhe për t'ju ndihmuar të planifikoni strategjitë e reagimit.

**Ruajeni informacionin që mblidhni në mënyrë të sigurt**. Është më e sigurt të ruani kopje rezervë në pajisjet tuaja (jo vetëm në internet ose "në re - cloud"). Mbrojini këta skedarë me enkriptim, dhe në dosje të fshehura ose ndarje të disqeve nëse është e mundur. Ju nuk do të dëshironit t'i humbni këto regjistra ose, më keq, të zbulohen ato.

Në mediat sociale, **shmangni bllokimin, heshtjen ose raportimin e llogarive që ju sulmojnë** derisa të keni mbledhur dokumentacionin që ju nevojitet, pasi kjo mund t'ju ndalojë të ruani prova nga ato llogari. Nëse tashmë keni bllokuar, heshtur ose raportuar llogari në mediat sociale, mos u shqetësoni. Ju mund të jeni në gjendje të zhbllokoni ose çaktivizoni heshtjen e llogarive që nuk mund t'i shihni më, ose dokumentimi mund të jetë ende i mundur duke e parë atë llogari nga llogaria e një miku ose kolegu.

## Merrni pamje nga ekrani

Mos e nënvlerësoni rëndësinë e marrjes së pamjeve nga ekrani të të gjitha sulmeve (ose të kërkoni nga dikush te i cili keni besim ta bëjë atë për ju). Shumë mesazhe digjitale janë të lehta për t'u fshirë ose për t'u humbur gjurmët. Disa, si mesazhet në Twitter ose WhatsApp, mund të rikuperohen vetëm me ndihmën e platformave, të cilave shpesh u duhet shumë kohë për t'iu përgjigjur raporteve të sulmeve, nëse përgjigjen fare.

Nëse nuk dini si të merrni një pamje nga ekrani në pajisjen tuaj, bëni një kërkim në internet për atë se "si të marr pamjen e ekranit" me markën, modelin dhe sistemin operativ të pajisjes suaj (për shembull, “Si të marr pamjen e ekranit në Samsung Galaxy S21 Android") .

Nëse jeni duke marrë një pamje ekrani të një mesazhi në një shfletues të internetit, është e rëndësishme që të **përfshini adresën (URL) të faqes në fotografinë tuaj të kapur**. Ajo gjendet në shiritin e adresave në krye të dritares së shfletuesit; ndonjëherë shfletuesit e fshehin dhe e shfaqin atë ndërsa lëvizni nëpër faqe. Adresat ndihmojnë për të verifikuar se ku ka ndodhur ngacmimi dhe ndihmojnë profesionistët teknikë dhe ligjorë ta lokalizojnë më lehtë atë. Më poshtë mund të shihni një shembull të një pamje ekrani që tregon adresën URL.

<img src="../../images/Screenshot_with_URL.png" alt="Here is a sample view of a screenshot showing the URL." width="80%" title="Here is a sample view of a screenshot showing the URL.">

<a name="legal"></a>
## Dokumentimi për një çështje ligjore

Mendoni nëse dëshironi të ndërmerrni veprime ligjore. A do t'ju ekspozojë ndaj rrezikut shtesë? A mund të përballoni kohën dhe përpjekjen që nevojitet për këtë? Fillimi i një çështjeje ligjore nuk është gjithmonë i detyrueshëm ose i domosdoshëm. Konsideroni gjithashtu nëse veprimet ligjore do t'ju bëjnë të ndiheni më të këndellur.

Nëse vendosni ta çoni çështjen në gjykatë, kërkoni këshilla nga një avokat ose organizatë ligjore të cilës i besoni. Këshilla e dobët ose e keqe ligjore mund të jetë stresuese dhe e dëmshme. Mos ndërmerrni veprime ligjore vetë nëse nuk e dini vërtet se çfarë po bëni.

Në shumicën e juridiksioneve, një avokat duhet të demonstrojë se provat janë të rëndësishme për çështjen, si janë marrë dhe verifikuar, se janë mbledhur në mënyrë të ligjshme, se mbledhja e saj ka qenë e nevojshme për ndërtimin e çështjes dhe fakte të tjera që i bëjnë provat të pranueshme për një gjykatë. Pasi avokati ta demonstrojë këtë, provat do të vlerësohen nga gjyqtari ose gjykata.

Gjërat që duhen marrë parasysh kur dokumentoni për një çështje ligjore:

* Është thelbësore të ruhen provat shpejt, që nga fillimi i një sulmi, sepse përmbajtja mund të fshihet shpejt nga ai që e ka postuar atë ose nga faqja e mediave sociale, duke e bërë më të vështirë ruajtjen e provave për veprime ligjore më vonë.
* Edhe nëse sulmi duket i vogël, ruajtja e të gjitha provave është e rëndësishme. Mund të jetë pjesë e një modeli që do të vëretohet se është kriminal vetëm kur ta shihni nga fillimi në fund.
* Mund të dëshironi të kërkoni nga platformat ose ofruesit e shërbimeve të internetit (ISP-të) që të ruajnë për ju informacionin në lidhje me sulmin digjital. Kjo mund të jetë e mundur vetëm për një kohë të shkurtër (disa javë ose një muaj) pasi ka ndodhur ngjarja. Shihni [informacionin për paraqitjen e një kërkese për ruajtjen e të dhënave të “Without My Consent”](https://withoutmyconsent.org/resources/something-can-be-done-guide/evidence-preservation/#consider-whether-to-include-a-litigation -hold-request) për më shumë informacion se si ta bëni këtë.
* Regjistrimet digjitale të kohës së ndodhjes së ngjarjeve të caktuara dhe adresat e e-mailit dhe të internetit janë thelbësore që provat të pranohen në gjykatë. Avokatët duhet të demonstrojnë se një mesazh kaloi nga një pajisje në tjetrën në një ditë të caktuar në një kohë të caktuar duke përdorur këto mjete.
* Avokatët gjithashtu duhet të vërtetojnë se provat nuk janë manipuluar, se ato kanë qenë në duar të sigurta që kur i keni ruajtur dhe se janë autentike. Pamjet e ekranit dhe printimet e e-maileve nuk konsiderohen prova mjaft të forta në këtë drejtim.

Për këto arsye ligjore, mund të jetë më mirë për ju që të angazhoni një ekspert, si p.sh. një noter ose një kompani të certifikimit digjital që mund të dëshmojë në gjykatë nëse është e nevojshme. Kompanitë e certifikimit digjital shpesh janë më pak të shtrenjta se certifikatat e noterizuara. Çdo noter ose kompani certifikimi që e angazhoni duhet të ketë njohuri të mjaftueshme teknike për të kryer detyra si këto nëse është e nevojshme:

* të konfirmojë regjistrimet digjitale të kohës së ndodhjes së ngjarjeve të caktuara.
* të vërtetojë ekzistencën ose mosekzistencën e përmbajtjes së caktuar, si p.sh. duke i krahasuar pamjet e ekranit dhe provat e tjera të mbledhura nga burimi origjinal, për të konfirmuar se provat tuaja nuk janë manipuluar
* të ofrojë dëshmi se certifikatat digjitale përputhen me URL-të
* të vërtetojë identitetet, të tilla si kontrolli nëse një person shfaqet në një listë profilesh në një platformë të mediave sociale dhe nëse ai zotëron një profil me pseudonimin që ju sulmon
* të konfirmojë pronësinë e një numri telefoni në një bisedë të zhvilluar në WhatsApp

Kini parasysh se jo të gjitha provat që keni mbledhur do të pranohen nga gjykata. Hulumtoni procedurat ligjore në lidhje me kërcënimet digjitale në vendin dhe rajonin tuaj për të vendosur se çfarë informacione do të mblidhni.

### Dokumentimi i shkeljeve të të drejtave të njeriut

Nëse keni nevojë të dokumentoni shkeljet e të drejtave të njeriut dhe të shkarkoni ose kopjoni materiale që platformat e mediave sociale i heqin në mënyrë aktive, mund të dëshironi të kontaktoni organizatat si [Mnemonic]( https://mnemonic.org/en/our-work) që mund të ndihmojnë për t’i ruajtur këto materiale.

## Si të ruani provat për raportet ligjore dhe ato të ekspertizës digjitale

Përveç ruajtjes së pamjeve të ekranit, këtu janë disa udhëzime të mëtejshme se si të ruani provat në mënyrë më të plotë për të mbështetur çështjen tuaj dhe punën e dikujt që ju jep mbështetje ligjore ose teknike.

* **Regjistrat e thirrjeve.** Numrat në thirrjet hyrëse dhe dalëse ruhen në një bazë të dhënash në telefonin tuaj celular.
    * Mund të bëni pamje nga ekrani i regjistrit.
    * Është gjithashtu e mundur të ruani këto regjistra duke bërë kopje rezervë të sistemit të telefonit tuaj ose duke përdorur softuer specifik për të shkarkuar regjistrat.
* **Mesazhet me tekst (SMS)**: Shumica e telefonave celularë ju lejojnë të ruani mesazhet tuaja SMS në re (cloud). Nga ana tjetër, mund të bëni edhe pamje nga ekrani i tyre. Asnjëra nga këto metoda nuk llogaritet si provë ligjore pa verifikim shtesë, por ato janë megjithatë të rëndësishme për dokumentimin e sulmeve.
    * Android: nëse telefoni juaj nuk ka aftësinë për të bërë kopje rezervë të mesazheve SMS, mund të përdorni një aplikacion të tillë si [SMS Backup & restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore).
    * iOS: Aktivizo ruajtjen e kopjeve rezervë të mesazheve këtu: iCloud te Cilësimet > [emri yt i përdoruesit] > Manage storage > Backup. Prekni emrin e pajisjes që po përdorni dhe aktivizoni kopjet rezervë të mesazheve.
* **Regjistrimi i thirrjes**. Celularët më të rinj vijnë me një regjistrues thirrjesh të instaluar. Kontrolloni se cilat janë rregullat ligjore në vendin tuaj ose në juridiksionin lokal për regjistrimin e telefonatave me ose pa pëlqimin e të dyja palëve në një telefonatë; në disa raste, mund të jetë i paligjshëm. Nëse është e ligjshme për ta bërë këtë, mund ta konfiguroni regjistruesin e telefonatave që të aktivizohet kur i përgjigjeni telefonatës, pa e paralajmëruar telefonuesin se biseda është duke u regjistruar.
    * Android: te Cilësimet > Telefoni > Cilësimet e regjistrimit të thirrjeve mund të aktivizoni regjistrimin automatik të të gjitha thirrjeve, thirrjeve nga numra të panjohur ose nga numra të caktuar. Nëse ky opsion nuk është në dispozicion në pajisjen tuaj, mund të shkarkoni një aplikacion të tillë si [Call Recorder](https://play.google.com/store/apps/details?id=com.lma.callrecorder).
    * iOS: Apple është më kufizues për sa i përket regjistrimit të thirrjeve dhe e ka bllokuar këtë opsion si parazgjedhje. Do t'ju duhet të instaloni një aplikacion të tillë si [RecMe](https://apps.apple.com/us/app/call-recorder-recme/id1455818490).
* **E-mailet**. Çdo e-mail ka një titull ("header") që përmban informacione se kush e dërgoi mesazhin dhe si, sikurse adresat dhe vula postare në një letër të dërguar me postë.
    * Për udhëzime mbi shikimin dhe ruajtjen e titujve të e-maileve, mund të përdorni [këtë udhëzues nga Qendra e Reagimit ndaj Incidenteve Kompjuterike në Luksemburg (CIRCL)](https://www.circl.lu/pub/tr-07/).
    * Nëse keni më shumë kohë, mund të dëshironi gjithashtu të konfiguroni e-mailin tuaj për të shkarkuar të gjitha mesazhet në një klient desktop si p.sh. Thunderbird duke përdorur [POP3](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3#w_changing-your-account). Udhëzimet për konfigurimin e një llogarie e-maili në Thunderbird mund të gjenden në [dokumentacionin zyrtar](https://support.mozilla.org/en-US/kb/manual-account-configuration).
* **Fotografitë**. Të gjitha fotografitë kanë "etiketa EXIF" - meta të dhëna që mund t'ju tregojnë se ku dhe kur është bërë një fotografi.
    * Ju mund të jeni në gjendje të ruani fotografitë nga një faqe interneti ose mesazhet që keni marrë në një mënyrë që ruan etiketat EXIF, të cilat do të humbisnin nëse do të merrnit vetëm një pamje të ekranit. Shtypni dhe mbani fotografinë në një pajisje celulare ose klikoni me të djathtën mbi fotografinë në kompjuter (kontrollo-kliko në një pajisje Mac, përdor tastin e menusë në Windows).
* **Ueb-faqet**. Shkarkimi ose bërja e kopjeve të plota rezervë të ueb-faqeve mund të jetë e dobishme për ata që përpiqen t'ju ndihmojnë.
    * Nëse faqja ku ka ndodhur ngacmimi është publike (me fjalë të tjera, nuk duhet të identifikoheni për ta parë atë), mund të futni adresën e faqes në Arkivin e internetit të [Wayback Machine](https://web.archive.org/) për të ruajtur faqen, për të regjistruar datën kur e keni ruajtur atë dhe për t’u kthyer më vonë në faqen e ruajtur për atë datë.
    * Pamjet e ekranit ofrojnë më pak detaje, por gjithashtu mund të ruajnë informacione të rëndësishme.
* **WhatsApp**. Bisedat në WhatsApp mund të shkarkohen në format teksti ose me media të bashkangjitur; ato gjithashtu ruhen si kopje rezervë si parazgjedhje në iCloud ose Google Drive.
    * Ndiqni udhëzimet për eksportimin e historisë së një bisede individuale ose grupore [këtu](https://faq.whatsapp.com/1180414079177245/?helpref=uf_share).
* **Telegram**. Për të eksportuar biseda në Telegram, do t'ju duhet të përdorni aplikacionin e desktopit. Ju mund të zgjidhni formatin dhe periudhën kohore që dëshironi të eksportoni dhe Telegrami do të gjenerojë një skedar HTML. Shihni udhëzimet [këtu](https://telegram.org/blog/export-and-more).
* **Facebook Messenger**. Hyni në llogarinë tuaj në Facebook. Shkoni te Cilësimet > Informacioni juaj në Facebook > Shkarkoni një kopje të informacionit tuaj. Do të shfaqen shumë opsione: zgjidhni "Mesazhet". Facebook-u merr kohë për të përpunuar skedarin, por do t'ju njoftojë kur të jetë i disponueshëm për shkarkim.
* Nëse duhet të ruani **videot** si provë, merrni një disk të jashtëm (një hard disk, USB ose kartë SD) me hapësirë të mjaftueshme ruajtëse. Mund t'ju duhet të përdorni një mjet për kapjen e ekranit të videos. Aplikacioni i kamerës, XBox Game Bar ose Snipping Tool mund të përdoren për këtë në Windows, Quicktime në Mac, ose mund të përdorni një shtojcë shfletuesi si [Video DownloadHelper](https://www.downloadhelper.net/) për të ruajtur videot.
* Në rastin e sulmeve të organizuara në shkallë të gjerë - për shembull, çdo gjë e postuar me një **hashtag specifik, ose një numër të madh komentesh në internet** - mund t'ju duhet ndihmë nga një ekip sigurie, laborator i ekspertizës digjitale ose universiteti për të mbledhur dhe përpunuar sasi shumë të madhe të të dhënave. Organizatat të cilat mund t’i kontaktoni për ndihmë përfshijnë [Citizen Lab](https://citizenlab.ca/about/), [K-Lab të Fondacionit Karisma](https://web.karisma.org.co/klab/) (punon në spanjisht), [Meedan](https://meedan.com/programs/digital-health-lab), Observatori i Internetit i Stanfordit dhe Instituti i Internetit i Oksfordit.
* **Skedarët e bashkangjitur mesazheve keqdashëse** janë prova të vlefshme. Në asnjë rrethanë nuk duhet t'i klikoni ose t'i hapni ato. Një këshilltar teknik i besuar duhet të jetë në gjendje t'ju ndihmojë t'i mbani në mënyrë të sigurt këto bashkëngjitje dhe t'ua dërgojë ato njerëzve që mund t'i analizojnë ato.
    * Mund të përdorni [Danger Zone](https://dangerzone.rocks/) për konvertimin e skedarëve PDF, dokumenteve të zyrës ose fotografive potencialisht të rrezikshme në PDF të sigurta.
* Për kërcënime më të synuara, të tilla si **spyware** në pajisjet tuaja ose **email-et keqdashëse** të dërguara posaçërisht për ju, mbledhja e provave teknike më të thella mund të ndihmojë çështjen tuaj. Është e rëndësishme të dini se kur t'i mblidhni vetë provat dhe kur t'ua lini këtë ekspertëve. Nëse nuk jeni të sigurt se si të vazhdoni, kontaktoni një [këshilltar të besuar](../support).
    * **Nëse pajisja është e ndezur, lëreni të ndezur. Nëse është e fikur, lëreni të fikur.** Duke e fikur ose ndezur, rrezikoni të humbni informacione të rëndësishme. Për hetime shumë të ndjeshme dhe ligjore, është më mirë të përfshini ekspertë përpara se ta fikni ose ndizni përsëri pajisjen.
    * Bëni fotografi të pajisjes që mendoni se përmban softuer keqdashës. Dokumentoni gjendjen e saj fizike dhe vendin ku e gjetët kur filluat të dyshoni se dikush i “ka futur duart pa leje”. A ka ndonjë gropësirë apo gërvishtje? A është e lagësht? A ka mjete aty pranë që mund të ishin përdorur për ta përdorur atë pa leje?
    * Mbani pajisjen dhe të dhënat që keni kopjuar prej saj në një vend të sigurt.
    * Metoda e saktë e nxjerrjes së të dhënave varet nga pajisja. Nxjerrja e të dhënave nga një laptop është ndryshe nga nxjerrja e të dhënave nga një smartfon. Çdo pajisje kërkon mjete dhe njohuri specifike.
* Mbledhja e të dhënave për kërcënimet e synuara shpesh përfshin kopjimin e **skedarëve të regjistrit të sistemit** që telefoni ose kompjuteri juaj i mbledh automatikisht. Ekspertët teknikë që ju ndihmojnë me mbledhjen e të dhënave mund t'i kërkojnë këto. Ata duhet t'ju ndihmojnë t'i mbani në mënyrë të sigurt meta të dhënat dhe bashkëngjitjet dhe t'ua dërgoni njerëzve që mund t'i analizojnë ato.
    * Për t’i ruajtur këto meta të dhëna, mbajeni pajisjen të izoluar nga sistemet e tjera të ruajtjes, fikni Wi-Fi-në dhe Bluetooth-in, shkëputni lidhjet e rrjetit me tela dhe mos modifikoni kurrë skedarët e regjistrit.
    * Mos e lidhni një USB dhe mos u përpiqni të kopjoni skedarët e regjistrave në të.
    * Këta skedarë mund të përfshijnë informacione rreth gjendjes së skedarëve në pajisje (si p.sh. mënyra se si janë aksesuar skedarët) ose për pajisjen (si p.sh. nëse është dhënë një komandë për fikjen e pajisjes ose për fshirje, ose nëse dikush ka tentuar të kopjojë skedarët në një pajisje tjetër).
