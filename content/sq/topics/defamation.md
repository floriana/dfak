﻿---
layout: page
title: "Unë jam në shënjestër të një fushate shpifjeje"
author: Inés Binder, Florencia Goldsman, Erika Smith, Gus Andrews
language: sq
summary: "Çfarë të bëni kur dikush përpiqet të dëmtojë reputacionin tuaj në internet."
date: 2023-04
permalink: /sq/topics/defamation
parent: Home
---

# Unë jam në shënjestër të një fushate shpifjeje

Kur dikush me qëllim krijon dhe shpërndan informacione të rreme, të manipuluara ose mashtruese për të shkaktuar dëm duke minuar reputacionin e dikujt, kjo është një fushatë shpifjeje. Këto fushata mund të synojnë individë ose organizata që kanë një lloj ekspozimi publik. Ato mund të orkestrohen nga çdo aktor, qoftë qeveritar apo privat.

Edhe pse ky nuk është një fenomen i ri, mjedisi digjital i përhap këto mesazhe me shpejtësi, duke rritur shkallën dhe shpejtësinë e sulmit dhe duke thelluar ndikimin e tij.

Ka një dimension gjinor të fushatave shpifëse kur qëllimi është të përjashtohen gratë dhe njerëzit LGBTQIA+ nga jeta publike, duke shpërndarë informacion që "shfrytëzon pabarazitë gjinore, promovon heteronormativitetin dhe i thellon ndarjet sociale" [[1]](#note-1).

Ky seksion i Veglërisë së Ndihmës së Parë Digjitale do t'ju udhëzojë nëpër disa hapa bazë për të planifikuar se si t'i përgjigjeni një fushate shpifjeje. Ndiqni këtë pyetësor për të identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhjet e mundshme.

<a name="note-1"></a>
[1] [Kundërvënia ndaj dezinformimit](https://counteringdisinformation.org/node/13/)

## Workflow

### physical-wellbeing

A keni frikë për sigurinë apo mirëqenien tuaj fizike?

- [Po](#physical-risk_end)
- [Jo](#no-physical-risk)

### no-physical-risk

> Një fushatë shpifjeje synon të sulmojë reputacionin tuaj duke e vënë në dyshim atë, duke hedhur dyshime, duke inkuadruar objektivin, duke pretenduar gënjeshtra ose duke ekspozuar kontradikta për të gërryer besimin e publikut.
>
> Sulme të tilla prekin veçanërisht njerëzit me ekspozim publik, puna e të cilëve varet nga prestigji dhe besimi i tyre: aktivistët dhe mbrojtësit e të drejtave të njeriut, politikanët, gazetarët dhe njerëzit që i vënë të dhënat në dispozicion të publikut, artistëve, etj.

A po përpiqet ky sulm të minojë reputacionin tuaj?

 - [Po](#perpetrators)
 - [Jo](#no-reputational-damage)

### no-reputational-damage

> Nëse jeni duke u përballur me një sulm që nuk synon të cenojë reputacionin tuaj, mund të përballeni me një emergjencë të një natyre tjetër në vend se me një fushatë shpifjeje.

A dëshironi t’u përgjigjeni pyetjeve për të diagnostikuar ngacmimin në internet?

 - [Po, mund të përballem me ngacmim në internet](../../../harassed-online)
 - [Jo, unë ende mendoj se bëhet fjalë për një fushatë shpifjeje](#perpetrators)
 - [Kam nevojë për mbështetje për të kuptuar problemin me të cilin po përballem](/../../support)


### perpetrators

A e dini se kush qëndron pas fushatës së shpifjes?

 - [Po](#respond-defamation)
 - [Jo](#analyze-messages)

### respond-defamation

> Ka shumë mënyra për t'iu përgjigjur një fushate shpifjeje, në varësi të ndikimit dhe përhapjes së mesazheve, aktorëve të përfshirë dhe motivimeve të tyre. Nëse fushata ka një ndikim dhe përhapje të ulët, ju rekomandojmë ta injoroni atë. Nëse, përkundrazi, ka një ndikim dhe përhapje të lartë, mund të konsideroni raportimin dhe heqjen e përmbajtjes, prezantimin e fakteve të vërteta dhe mbylljen e saj me heshtje.
>
> Kini kujdes që të mos përforconi mesazhet shpifëse kur përpiqeni të adresoni fushatën e shpifjes. Citimi i një mesazhi, qoftë edhe vetëm për të ekspozuar njerëzit ose motivet pas tij, mund të rrisë përhapjen e tij. Merrni parasysh ["teknika e sanduiçit të së vërtetës"](https://en.wikipedia.org/wiki/Truth_sandwich) në mënyrë që të mbuloni informacionin e rremë pa e përhapur më tej pa dashje. Kjo teknikë "përfshin prezantimin e së vërtetës për një temë përpara se të mbulojë dezinformatat, pastaj duke e përfunduar storjen duke e paraqitur përsëri të vërtetën".
>
> Zgjidhni strategjitë që ju duken të përshtatshme për të frenuar sulmin, për të rindërtuar besimin dhe për të rivendosur besueshmërinë në komunitetin tuaj. Mos harroni se pavarësisht se çfarë strategjie do të zgjidhni, [kujdesi për veten](/../../self-care/) tuaj duhet të jetë prioriteti kryesor. Konsideroni gjithashtu [dokumentimin](/../../documentation) e përmbajtjes ose të profileve që po ju sulmojnë përpara se të përgjigjeni.

Si dëshironi t'i përgjigjeni fushatës së shpifjes?

 - [Dua të njoftoj platformat e mediave sociale dhe të heq përmbajtjen shpifëse](#notify-take-down)
 - [Dua ta injoroj atë dhe të hesht njoftimet](#ignore-silence)
 - [Dua t’i prezantoj faktet e vërteta](#set-record-straight)
 - [Dua të paraqes një ankesë ligjore](#legal-complaint)
 - [Më duhet të analizoj më tej mesazhet shpifëse që të marr një vendim](#analyze-messages)

### analyze-messages

> Kur ju ose organizata juaj jeni objektivi i një fushate shpifjeje, të kuptuarit e metodave standarde të përdorura për përhapjen e dezinformatave mund t'ju ndihmojë të peshoni rreziqet dhe të orientoni hapat e ardhshëm. Analizimi i mesazheve që merrni gjatë këtij lloj sulmi mund t'ju japë informacion të mëtejshëm rreth autorëve, motivimit dhe burimeve të përdorura për të minuar reputacionin tuaj. Teknikat e sulmit përfshijnë një sërë mjetesh dhe koordinim online.
>
> Ju mund të dëshironi së pari të vlerësoni nivelin e rrezikut me të cilin po përballeni. Në [Paketën për ndërveprim me dezinformatat](https://www.interaction.org/documents/disinformation-toolkit/), do të gjeni një Mjet për vlerësimin e rrezikut për t'ju ndihmuar të vlerësoni cenueshmërinë e mjedisit tuaj mediatik.
>
> Një fushatë shpifjeje në mediat sociale do të përdorë shpesh hashtag-e për të tërhequr interes më të madh, por kjo është gjithashtu e dobishme për monitorimin e sulmit pasi mund të kërkoni sipas hashtag-ut dhe të vlerësoni autorët dhe mesazhet. Për të matur ndikimin e hashtag-eve në mediat sociale, po të dëshironi mund të provoni këto mjete:
>
> - [Track my hashtag](https://https://www.trackmyhashtag.com/)
> - [Brand mentions](https://brandmentions.com/hashtag-tracker) grumbullon përmendjet në hashtag-e nga Twitter-i, Instagram-i dhe Facebook-u në një rrjedhë të vetme të dhënash.
> - [InVid Project](https://www.invid-project.eu/) është një platformë verifikimi njohurish për të zbuluar storjet e reja dhe për të vlerësuar besueshmërinë e skedarëve video dhe përmbajtjeve që ia vlen të bëhen lajme, që përhapen përmes mediave sociale.
> - [Metadata2go](https://www.metadata2go.com/view-metadata) zbulon meta të dhënat pas skedarëve që i analizoni.
> - [YouTube DataViewer](https://citizenevidence.amnestyusa.org/) mund të përdoret për të bërë një kërkim të kundërt të videove të Amnesty International për të parë nëse videoja është më e vjetër dhe nëse është modifikuar.
>
> **Sulmet manuale kundrejt sulmeve të automatizuara**
>
> Aktorët shtetërorë dhe kundërshtarët e tjerë shpesh përdorin botnet-e të koordinuara të cilat nuk janë aq të kushtueshme ose që janë teknikisht të vështira për t'u vendosur. Njohja se një breshëri mesazhesh sulmi nuk janë nga dhjetëra apo qindra njerëz, por janë bot-e të automatizuar, mund të zvogëlojë ankthin dhe ta bëjë dokumentimin më të lehtë dhe do t'ju ndihmojë të vendosni se si dëshironi të mbroni veten. Përdorni [Botsentinel](https://botsentinel.com/) për të kontrolluar natyrën e llogarive që ju sulmojnë. Disa mesazhe bot mund të merren dhe të shpërndahen më tej edhe nga individë, kështu që mund të zbuloni se rezultati verifikon disa llogari që transmetojnë mesazhe dezinformuese, p.sh. ata kanë ndjekës, një foto dhe përmbajtje unike të profilit dhe tregues të tjerë që do të thotë se ka më pak probabilitet që ato të jenë një llogari bot. Një mjet tjetër që mund të përdorni është [Pegabot](https://es.pegabot.com.br/).
>
> **Informacioni i brendshëm kundrejt informacionit publik**
>
> Dezinformimi i shkathtë mund të bazojë përmbajtjen në një bërthamë të së vërtetës ose të bëjë që përmbajtja të duket e vërtetë. Reflektimi mbi llojin e informacionit që ndahet dhe burimet e tij mund të përcaktojë drejtime të ndryshme veprimi.
>
> A është informacioni privat apo nga burime të brendshme? Merrni parasysh ndryshimin e fjalëkalimeve tuaja dhe instalimin e vërtetimit me dy faktorë për t'u siguruar që askush tjetër nuk ka qasje në llogaritë tuaja përveç jush. Nëse ky informacion referohet në çate ose në biseda ose skedarë të përbashkët, inkurajoni miqtë dhe kolegët të rishikojnë cilësimet e tyre të sigurisë. Merrni parasysh pastrimin dhe mbylljen e çateve (bisedave) të vjetra në mënyrë që informacionet nga e kaluara të mos jenë lehtësisht të aksesueshme në pajisjen tuaj ose të kolegëve dhe miqve tuaj.
>
> Kundërshtarët mund të hakojnë llogaritë e një individi ose një organizate për të përhapur [informata të rreme ose të pasakta](https://en.wikipedia.org/wiki/Misinformation) dhe [dezinformata](https://en.wikipedia.org/wiki/Disinformation) nga ajo llogari, duke i dhënë informacionit që ndahet shumë më tepër legjitimitet. Për të rifituar kontrollin e llogarive tuaja, shikoni seksionin e Veglërisë së Ndihmës së Parë Digjitale [Nuk mund të hyj në llogarinë time](../../../account-access-issues/questions/What-Type-of-Account-or- Service/).
>
> A është informacioni që shpërndahet me burim nga informacioni publik i disponueshëm në internet? Mund të jetë e dobishme të ndiqni seksionin e Veglërisë së Ndihmës së Parë Digjitale [Unë kam qenë cak i doxing-ut (veprimi ose procesi i kërkimit dhe publikimit të informacionit privat ose identifikues për një individ të caktuar në internet, zakonisht me qëllim keqdashës) ose dikush po ndan fotografi të mia pa pëlqimin tim](../../../doxing) për të kërkuar dhe ngushtuar hapësirën prej ku u bë i disponueshëm ky informacion dhe çfarë mund të bëhet.
> <br />
>
> **Krijimi i materialit më të sofistikuar** (video në YouTube, deepfake - video të rreme të fytyrës së dikujt, etj.)
>
> Ndonjëherë sulmet janë më të shënjestruara dhe autorët kanë investuar kohë dhe përpjekje për të krijuar fotografi dhe lajme të rreme në videot e YouTube ose [deepfakes](https://en.wikipedia.org/wiki/Deepfake) në mënyrë që të mbështesin storjen e tyre të rreme. Përhapja e një materiali të tillë mund të zbulojë autorët dhe të japë më shumë material për denoncimin ligjor dhe publik të sulmit dhe arsye të qarta për kërkesat për heqje të përmbajtjes nga interneti. Mund të nënkuptojë gjithashtu se sulmet janë shumë të personalizuara dhe se rrisin ndjenjat e cenueshmërisë dhe rrezikut. Ashtu si kur përballeni me ndonjë sulm tjetër, [kujdesi për veten](/../../self-care/) dhe siguria personale janë prioritetet kryesore.
>
> Një shembull tjetër është përdorimi i domeneve të rreme në të cilat një kundërshtar krijon një faqe interneti ose profil në një rrjet social që duket i ngjashëm me llogarinë autentike. Nëse jeni duke u përballur me një problem të tillë, shikoni seksionin e Veglërisë së Ndihmës së Parë Digjitale [prezantimi i rrejshëm](../../../impersonated/).
>
> Ky lloj sulmi mund të tregojë gjithashtu se ka organizim më të madh, logjistikë, financim dhe kohë të shpenzuar nga ana e sulmuesit, duke dhënë një pasqyrë më të qartë në lidhje me motivimin e sulmit dhe autorët e mundshëm.

A e keni tani një ide më të mirë se kush po ju sulmon dhe si po e bëjnë këtë?

 - [Po, tani do të doja të shqyrtoja një strategji për të reaguar](#respond-defamation)
 - [Jo, kam nevojë për mbështetje për të kuptuar se si të reagoj](#defamation_end)

### notify-take-down

> ***Shënim:*** *Merrni parasysh [dokumentimin e sulmeve](/../../documentation) përpara se të kërkoni që një platformë të heqë përmbajtjen shpifëse. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/../../documentation#legal).*
>
> Kushtet e shërbimit të platformave të ndryshme do të shpjegojnë kur kërkesat për heqje të përmbajtjes do të konsiderohen legjitime, p.sh. përmbajtje që shkel të drejtën e autorit, pronar i së cilës duhet të jeni ju. Pronësia e të drejtës së autorit ndodh me automatizëm që nga momenti kur krijoni një vepër origjinale: bëni një fotografi, shkruani një tekst, kompozoni një këngë, filmoni një video, etj. Nuk mjafton që ju të paraqiteni në vepër; ju duhet ta keni krijuar atë.

A i zotëroni të drejtat për përmbajtjen e përdorur në fushatën e shpifjes?

 - [Po](#copyright)
 - [Jo](#report-defamatory-content)

### set-record-straight

> Versioni juaj i fakteve është gjithmonë i rëndësishëm. Prezantimi i fakteve të vërteta munnd të jetë më urgjent nëse fushata e shpifjes kundër jush është e përhapur dhe shumë e dëmshme për reputacionin tuaj. Megjithatë, në këto raste, ju duhet të vlerësoni ndikimin e fushatës mbi ju për të vlerësuar nëse një përgjigje publike është strategjia juaj më e mirë.
>
> Përveç raportimit të përmbajtjes dhe aktorëve të këqij në platforma, mund të dëshironi të ndërtoni një kundër-narrativë, t’i demaskoni dezinformatat, të denonconi publikisht autorët ose thjesht të paraqitni të vërtetën. Mënyra se si do të përgjigjeni do të varet gjithashtu nga komuniteti që ju mbështet për t'ju ndihmuar dhe nga vlerësimi juaj i rrezikut me të cilin mund të përballeni për shkak të marrjes së një qëndrimi publik.

A keni një ekip njerëzish për t'ju mbështetur?

 - [Po](#campaign-team)
 - [Jo](#self-campaign)

### legal-complaint

> Kur përballeni me një proces ligjor, gjatë mbledhjes, organizimit dhe paraqitjes së provave në Gjykatë, ju duhet të ndiqni një protokoll specifik në mënyrë që ato të mund të pranohen nga gjyqtari si prova. Kur paraqitni prova në internet të një sulmi, nuk mjafton marrja e pamjes nga ekrani (screenshot). Shikoni seksionin e Veglërisë së Ndihmës së Parë Digjitale për atë se [si të dokumentoni një sulm për t'u paraqitur në një proces ligjor](/../../documentation#legal) për të mësuar më shumë rreth këtij procesi përpara se të kontaktoni një avokat sipas zgjedhjes suaj.

Çfarë do të dëshironit të bënit?

 - [Këto rekomandime ishin të dobishme - e di se çfarë të bëj tani](#resolved_end)
 - [Këto rekomandime ishin të dobishme, por unë do të doja të shqyrtoja një strategji tjetër](#respond-defamation)
 - [Kam nevojë për mbështetje për të vazhduar me ankesën time ligjore](#legal_end)

### ignore-silence

> Ka raste, veçanërisht në rastet kur fushata e shpifjes ka pak ndikim apo përhapje, kur ju mund të preferoni të injoroni autorët dhe thjesht t'i heshtni mesazhet. Ky mund të jetë gjithashtu një hap i rëndësishëm për kujdesin ndaj vetes dhe miqtë ose aleatët mund të regjistrohen për të monitoruar përmbajtjen për t'ju njoftuar nëse ka nevojë të ndërmerren veprime të tjera. Kjo nuk e eliminon përmbajtjen, veçanërisht nëse dëshironi të dokumentoni ose rishikoni më vonë informacionin që shpërndahet për t'ju dëmtuar.

Ku po shpërndahet përmbajtja shpifëse që dëshironi ta heshtni?

 - [E-mail](#silence-email)
 - [Facebook](#silence-facebook)
 - [Instagram](#silence-instagram)
 - [Reddit](#silence-reddit)
 - [TikTok](#silence-tiktok)
 - [Twitter](#silence-twitter)
 - [Whatsapp](#silence-whatsapp)
 - [YouTube](#silence-youtube)

### copyright

> Ju mund të përdorni rregulloret për të drejtat e autorit, të tilla si [Ligji i të Drejtave të Autorit i Mijëvjeçarit Digjital](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), për të hequr përmbajtjen. Shumica e platformave të mediave sociale ofrojnë forma për të raportuar shkeljen e të drejtave të autorit dhe mund të jenë më të gatshme të përgjigjen ndaj këtij lloji të kërkesave sesa opsionet e tjera për heqjen e përmbajtjes, për shkak të implikimeve ligjore për to.
>
> ***Shënim:*** *Gjithmonë [dokumentoni](/../../documentation) përpara se të kërkoni të hiqet përmbajtja. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/../../documentation#legal).*
>
> Ju mund t'i përdorni këto lidhje për të dërguar një kërkesë për heqjen e përmbajtjes për shkak të shkeljes së të drejtave të autorit në platformat kryesore të rrjeteve sociale:
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/en/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është hequr përmbajtja?

 - [Po](#resolved_end)
 - [Jo, kam nevojë për mbështetje ligjore](#legal_end)
 - [Jo, kam nevojë për mbështetje për të kontaktuar platformën](#harassment_end)

### report-defamatory-content

> Jo të gjitha platformat kanë procese të specializuara për raportimin e përmbajtjes shpifëse dhe disa prej tyre kërkojnë një proces paraprak ligjor. Më poshtë mund të shihni rrugën që çdo platformë e përcakton për raportimin e përmbajtjes shpifëse.
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/contact/430253071144967)
> - [Instagram](https://help.instagram.com/contact/653100351788502)
> - [TikTok](https://www.tiktok.com/legal/report/feedback)
> - [Twitch](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=en_US)
> - [Twitter](https://help.twitter.com/en/rules-and-policies/twitter-report-violation#directly)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885?helpref=search&cms_platform=android)
> - [YouTube](https://support.google.com/youtube/answer/6154230)
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është raportuar dhe analizuar përmbajtja për heqje nga ofruesi i shërbimit?

 - [Po](#resolved_end)
 - [Jo, kam nevojë për mbështetje ligjore](#legal_end)
 - [Jo, kam nevojë për mbështetje për të kontaktuar platformën](#harassment_end)

### silence-email

> Shumica e klientëve të postës elektronike ofrojnë opsionin për të filtruar mesazhet dhe për t'i etiketuar automatikisht ato, për t'i arkivuar ose për t'i fshirë në mënyrë që të mos shfaqen në kutinë tuaj mbërritëse. Zakonisht mund të krijoni rregulla për të filtruar mesazhet sipas dërguesit, marrësit, datës, madhësisë, bashkëngjitjes, titullit ose fjalëve kyçe.
>
> Mësoni se si të krijoni filtra në ueb-mail-e:
>
> - [Gmail](https://support.google.com/mail/answer/6579?hl=en#zippy=%2Ccreate-a-filter)
> - [Protonmail](https://proton.me/support/email-inbox-filters)
> - [Yahoo](https://help.yahoo.com/kb/SLN28071.html)
>
> Mësoni se si të krijoni filtra në klientët e postës elektronike:
>
> - [MacOS Mail](https://support.apple.com/guide/mail/filter-emails-mlhl1f6cf15a/mac)
> - [Microsoft Outlook](https://support.microsoft.com/en-us/office/set-up-rules-in-outlook-75ab719a-2ce8-49a7-a214-6d62b67cbd41)
> - [Thunderbird](https://support.mozilla.org/en-US/kb/organize-your-messages-using-filters)

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)

### silence-twitter

> Nëse nuk dëshironi të ekspozoheni ndaj mesazheve specifike, mund të bllokoni përdoruesit ose t'i heshtni përdoruesit dhe mesazhet. Mos harroni se nëse zgjidhni të bllokoni dikë ose disa përmbajtje, nuk do të keni akses në përmbajtjen shpifëse për të [dokumentuar sulmin](/../../documentation). Në këtë rast, mund të preferoni të keni akses në të përmes një llogarie tjetër ose thjesht ta heshtni atë.
>
> - [Hiqni (mute) llogaritë që të mos shfaqen në kronologjinë tuaj](https://help.twitter.com/en/using-twitter/twitter-mute)
> - [Heshtni (mute) fjalët, shprehjet, emrat e përdoruesve, emoji-t, hashtag-et dhe njoftimet për bisedë](https://help.twitter.com/en/using-twitter/advanced-twitter-mute-options)
> - [Shtyni përgjithmonë njoftimet për mesazhet direkte](https://help.twitter.com/en/using-twitter/direct-messages#snooze)

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)

### silence-facebook

> Megjithëse Facebook-u nuk ju lejon të heshtni (mute) përdoruesit ose përmbajtjen, ai ju lejon të bllokoni profilet e përdoruesve dhe faqet.
>
> - [Blloko një profil në Facebook](https://www.facebook.com/help/)
> - [Blloko mesazhet nga një profil në Facebook](https://www.facebook.com/help/1682395428676916)
> - [Blloko një faqe në Facebook](https://www.facebook.com/help/395837230605798)
> - [Ndalo ose blloko profilet nga faqja jote në Facebook](https://www.facebook.com/help/185897171460026/)

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)

### silence-instagram

> Këtu është një listë e këshillave dhe mjeteve për të heshtur përdoruesit dhe bisedat në Instagram:
>
> - [Heshtni ose çaktivizoni heshtjen e dikujt në Instagram](https://help.instagram.com/469042960409432)
> - [Heshtni ose çaktivizoni heshtjen e storjes së dikujt në Instagram](https://help.instagram.com/290238234687437)
> - [Kufizo ose çaktivizo kufizimin e dikujt](https://help.instagram.com/2638385956221960)
> - [Fikni sugjerimet e llogarisë për profilin tuaj në Instagram](https://help.instagram.com/530450580417848
> - [Fshih postimet e sugjeruara në postime që paraqiten në llogarinë tuaj Instagram (Instagram feed)](https://help.instagram.com/423267105807548)
> - [Fikni sugjerimet e llogarisë për profilin tuaj në Instagram](https://help.instagram.com/530450580417848

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)

### silence-youtube

> Nëse hyni në YouTube nga shfletuesi juaj i desktopit, mund të fshini videot, kanalet, seksionet dhe listat e videove nga faqja juaj kryesore.
>
> - [Përshtatni rekomandimet tuaja në YouTube](https://support.google.com/youtube/answer/6342839)
>
> Mund të krijoni gjithashtu një listë bllokimi në profilin tuaj në YouTube për të bllokuar shikuesit e tjerë në çatin tuaj të drejtpërdrejtë në YouTube.
>
> - YouTube: [myaccount.google.com/blocklist](https://myaccount.google.com/blocklist)
> - [Blloko shikuesit e tjerë në çatin e drejtpërdrejtë në YouTube](https://support.google.com/youtube/answer/7663906)

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)

### silence-whatsapp

> WhatsApp është një platformë e zakonshme për fushatat shpifëse. Mesazhet dërgohen shpejt për shkak të marrëdhënieve të besueshme midis dërguesve dhe marrësve.
>
> - [Arkivoni ose hiqni nga arkivi një çat ose grup](https://faq.whatsapp.com/1426887324388733)
> - [Dilni dhe fshini grupet](https://faq.whatsapp.com/498814665492149)
> - [Bllokoni një kontakt](https://faq.whatsapp.com/1142481766359885)
> - [Heshtni ose çaktivizoni heshtjen e njoftimeve të grupit](https://faq.whatsapp.com/694350718331007)

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)

### silence-tiktok

> TikTok ju lejon të bllokoni përdoruesit, të parandaloni përdoruesit të komentojnë videot tuaja, të krijoni filtra për komentet në videot tuaja dhe të fshini, heshtni dhe filtroni mesazhet e drejtpërdrejta.
>
> - [Bllokimi i përdoruesve në TikTok](https://support.tiktok.com/en/using-tiktok/followers-and-following/blocking-the-users)
> - [Zgjidhni se kush mund të komentojë videot tuaja në cilësimet tuaja](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments)
> - [Si të aktivizoni filtrat e komenteve për videot tuaja në TikTok](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments#3)
> - [Menaxhoni cilësimet e privatësisë së komenteve për të gjitha videot e tuaja në Tik Tok](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments#4)
> - [Menaxho cilësimet e privatësisë së komenteve për një nga videot e tuaja në TikTok](https://support.tiktok.com/en/using-tiktok/messaging-and-notifications/comments#5)
> - [Si të fshini, heshtni dhe filtroni mesazhet direkte](https://support.tiktok.com/en/account-and-privacy/account-privacy-settings/direct-message#6)
> - [Si të menaxhoni se kush mund t'ju dërgojë mesazhe direkte](https://support.tiktok.com/en/account-and-privacy/account-privacy-settings/direct-message#6)

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)

### silence-reddit

> Nëse shpifja qarkullon në Reddit, mund të zgjidhni të përjashtoni komunitetet ku kjo po ndodh të mos shfaqen në njoftimet tuaja, postimet në faqen tuaj kryesore (home feed) (duke përfshirë edhe rekomandimet) dhe postimet e popullarizuara (popular feed) në Reddit ose në faqen tuaj të desktopit Reddit. Nëse jeni moderator, mund t'i ndaloni ose heshtni gjithashtu përdoruesit që shkelin vazhdimisht rregullat e komunitetit tuaj.
>
> - [Heshtja e komunitetit në Reddit](https://reddit.zendesk.com/hc/en-us/articles/9810475384084-What-is-community-muting-)
> - [Ndalimi dhe heshtja e anëtarëve të komunitetit](https://mods.reddithelp.com/hc/en-us/articles/360009161872-User-Management-banning-and-muting)

A dëshironi të heshtni përmbajtjen shpifëse në një platformë tjetër?

 - [Po](#ignore-silence)
 - [Jo](#damage-control)


### campaign-team

> Nëse fushata kundër jush po investon kohë dhe burime, kjo sugjeron që autorët kanë një motivim të thellë për t'ju sulmuar. Mund t'ju duhet të përdorni të gjitha strategjitë e përmendura në këtë rrjedhë pune për të zbutur dëmin dhe për të ndërtuar një fushatë publike në mbrojtjen tuaj. Miqtë dhe aleatët mund të jenë të dobishëm dhe të përqendrojnë kujdesin tuaj për veten nëse ju mbështesin me analizën e mesazheve, dokumentimin e sulmeve dhe raportimin e përmbajtjes dhe profileve.
>
> Një ekip fushate mund të punojë së bashku për të vendosur mënyrën më të mirë për të kundërshtuar narrativën që përdoret kundër jush. Mesazhet e dezinformatave janë tinëzare dhe shpesh na thuhet të mos i përsërisim mesazhet e sulmuesve. Gjetja e një ekuilibri midis ngritjes së storjes suaj dhe hedhjes poshtë të pretendimeve të rreme është e rëndësishme.
>
> Është jetike që të bëni një [analizë të vlerësimit të rrezikut](https://www.interaction.org/documents/disinformation-toolkit/) për veten tuaj dhe për çdo individ dhe organizatë mbështetëse përpara se të filloni një fushatë publike.
>
> Përveç analizimit të mesazheve sulmuese, ju dhe ekipi juaj mund të dëshironi të filloni duke [hartuar organizatat dhe aleatët](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1. html) që e njohin sfondin tuaj dhe do të shtojnë një narrativ pozitiv për punën tuaj, ose do të bëjnë mjaft zhurmë gazmore për të shmangur ose ngulfatur një fushatë shpifjeje.
>
> Ju dhe organizata juaj mund të mos jeni të vetmit të sulmuar. Merrni parasysh ndërtimin e besimit me organizatat dhe komunitetet e tjera që janë diskriminuar ose synuar nga fushata e dezinformimit. Kjo mund të nënkuptojë përgatitjen e anëtarëve të komunitetit për të adresuar mesazhet e diskutueshme ose shkëmbimin e burimeve në mënyrë që të tjerët të mund të mbrohen.
>
> Ndani kohë në ekipin tuaj për të identifikuar se çfarë duan të provokojnë mesazhet dhe llojet e përmbajtjes suaj - gjithashtu duke u bazuar në faktin nëse jeni duke hedhur poshtë fushatën ose duke shpërndarë mesazhe pozitive pa iu referuar taktikave të shpifjes. Ju mund të dëshironi që njerëzit të jenë më të aftë në dallimin e thashethemeve nga faktet. Mesazhet e thjeshta të bazuara në vlera janë veçanërisht efektive në çdo fushatë publike.
>
> Përveç dhënies së deklaratave publike ose për shtypin, mund të dëshironi të zgjidhni dhe të përgatisni zëdhënës të ndryshëm për fushatën tuaj, gjë që ndihmon gjithashtu në shmangien e sulmeve të personalizuara të zakonshme në fushatat e shpifjeve, veçanërisht në rastet e dezinformimit gjinor.
>
> Krijimi i një sistemi të monitorimit të medias për të gjurmuar fushatën tuaj të mesazheve dhe atë të sulmuesve do t'ju ndihmojë të shihni se cilat mesazhe janë më efektive për ta përshtatur fushatën tuaj.
>
> Nëse përmbajtja shpifëse publikohet gjithashtu në një gazetë ose revistë, ekipi juaj mund të dëshirojë të kërkojë [Të drejtën e reagimit](#self-strategies) ose të aktivizojë çështjen tuaj përmes [faqeve për verifikimin e fakteve](#self-strategies).

A mendoni se keni qenë në gjendje të kontrolloni dëmtimin e reputacionit tuaj?

 - [Po](#resolved_end)
 - [Jo](#defamation_end)

### self-campaign

> Nëse nuk keni mbështetjen e një ekipi, duhet të merrni parasysh të gjitha kapacitetet ose burimet e kufizuara që i keni në dispozicion ndërsa planifikoni hapat e ardhshëm, duke i dhënë përparësi [kujdesit për veten tuaj (/../../self-care). Ju mund të dëshironi të publikoni një deklaratë në profilin tuaj në mediat sociale ose në faqen tuaj të internetit. Sigurohuni që të keni [kontroll të plotë](../../../account-access-issues) të atyre llogarive përpara se ta bëni këtë. Në deklaratën tuaj, ju mund të dëshironi t'i referoni njerëzit në trajektoren tuaj të vendosur dhe në burimet e besueshme që e pasqyrojnë atë trajektore. [Analiza juaj e fushatës së shpifjes](#analyze-messages) do të japë informacion mbi llojin e deklaratës që dëshironi të bëni. Për shembull, mund të rishikoni nëse ndonjë burim lajmi është duke u referuar në sulm.

A botohet përmbajtja shpifëse edhe në një gazetë apo revistë?

 - [Po](#self-strategies)
 - [Jo](#damage-control)

### vetë-strategji

> Nëse informacioni shpifës publikohet, politika redaktuese apo edhe ligji në një vend të caktuar mund t'ju japë [të drejtën e përgjigjes ose të drejtën e korrigjimit](https://en.wikipedia.org/wiki/Right_of_reply). Kjo jo vetëm që ju jep një platformë tjetër për të publikuar deklaratën tuaj, përtej mediave tuaja - ajo gjithashtu ofron një vend për t'u lidhur me deklaratat e tjera që qarkulloni.
>
> Nëse media e lajmeve që përmban informacion shpifës nuk është një burim i besueshëm lajmesh ose është shpesh burim lajmesh të pabaza, kjo mund të jetë e dobishme për t'u theksuar në çdo deklaratë.
>
> Ka shumë verifikues faktesh lokalë, rajonalë dhe globalë që mund t'ju ndihmojnë të demaskoni informacionin që qarkullon për ju.
>
> - [DW Fact Check](https://www.dw.com/en/fact-check/t-56584214)
> - [France24 LesObservateurs](https://observers.france24.com/fr/)
> - [AFP FactCheck](https://factcheck.afp.com/)
> - [EUvsDisinfo](https://euvsdisinfo.eu/)
> - [Latam Chequea](https://chequeado.com/latamchequea)
> - [Europa fact check ](https://eufactcheck.eu/)

A mendoni se keni nevojë për një përgjigje më të madhe ndaj fushatës së shpifjes?

 - [Po](#campaign-team)
 - [Jo](#damage-control)

### damage-control

> Analiza juaj e fushatës së shpifjes do të japë një pasqyrë të llojit të veprimeve që dëshironi të ndërmerrni. Për shembull, nëse mesazhet “bot” përdoren kundër jush, ju mund të dëshironi ta vini në dukje këtë në një deklaratë publike ose të sugjeroni motive të fshehta të autorëve që qëndrojnë pas fushatës nëse jeni në dijeni të tyre. Sidoqoftë, vendosja e trajektores suaj dhe marrja e ndihmës nga miqtë dhe aleatët për të promovuar dhe paraqitur të vërtetën tuaj mund të jetë preferenca juaj, në vend që të ndërveproni dhe të reagoni ndaj përmbajtjeve shpifëse.

A mendoni se keni qenë në gjendje të kontrolloni dëmtimin e reputacionit tuaj?

 - [Po](#resolved_end)
 - [Jo](#defamation_end)

### physical-risk_end

> Nëse jeni në rrezik fizik, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=physical_security)

### defamation_end

> Nëse jeni ende duke përjetuar dëmtim të reputacionit nga një fushatë shpifjeje, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=advocacy&services=individual_care&services=legal)

### harassment_end

> Nëse keni nevojë për mbështetje për të hequr përmbajtjen, mund të kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=harassment)

### legal_end

> Nëse jeni ende duke përjetuar dëmtim të reputacionit nga një fushatë shpifjeje dhe keni nevojë për mbështetje ligjore, ju lutemi kontaktoni organizatat e mëposhtme.

:[](organisations?services=legal)

### resolved_end

Shpresojmë se ky udhëzues për zgjidhjen e problemeve ishte i dobishëm. Ju lutemi jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Një mënyrë e rëndësishme për të kontrolluar dëmin e një plani shpifjeje është të keni praktika të mira sigurie për t'i mbajtur llogaritë tuaja të sigurta nga aktorët keqdashës dhe të matni pulsin e informacionit që qarkullon për ju ose organizatën tuaj. Ju mund të dëshironi të merrni parasysh disa nga këshillat parandaluese më poshtë.

- Hartëzoni praninë tuaj në internet. Vetë-doksimi konsiston në eksplorimin e inteligjencës me burim të hapur mbi veten për të parandaluar aktorët keqdashës që të gjejnë dhe përdorin këtë informacion për t'ju imituar.
- Konfiguroni paralajmërimet e Google. Ju mund të merrni e-mail kur shfaqen rezultate të reja për një temë në "Kërko me Google". Për shembull, mund të merrni informacion në lidhje me përmendjet e emrit tuaj ose të organizatës/emrit të kolektivit tuaj.
- Aktivizoni vërtetimin me dy faktorë (2FA) për llogaritë tuaja më të rëndësishme. 2FA ofron siguri më të madhe të llogarisë duke ju kërkuar të përdorni më shumë se një metodë për të hyrë në llogaritë tuaja. Kjo do të thotë që edhe nëse dikush do të merrte fjalëkalimin tuaj kryesor, ai/ajo nuk do të mund të hynte në llogarinë tuaj nëse nuk kishte gjithashtu telefonin tuaj celular ose një mjet tjetër dytësor vërtetimi.
- Verifikoni profilet tuaja në platformat e rrjeteve sociale. Disa platforma ofrojnë një veçori për verifikimin e identitetit tuaj dhe lidhjen e tij me llogarinë tuaj.
- Kapni pamjen e faqes suaj të internetit siç duket tani për ta përdorur si provë në të ardhmen. Nëse faqja juaj e internetit lejon “zvarritës”, mund të përdorni Wayback Machine, të ofruar nga archive.org. Vizitoni Wayback Machine të Internet Archive, futni emrin e faqes suaj të internetit në fushën nën titullin "Save Page Now" dhe klikoni butonin "Save Page Now".

#### resources

- [Unioni i Shkencëtarëve të Shqetësuar: Si të kundërshtoni dezinformimin: Strategjitë e komunikimit, praktikat më të mira dhe grackat për t'u shmangur](https://www.ucsusa.org/resources/how-counter-disinformation)
- [Adresimi i mizogjinisë në internet dhe dezinformimit gjinor: Një udhëzues se si të veprohet, nga ND](https://www.ndi.org/sites/default/files/Addressing%20Gender%20%26%20Disinformation%202%20%281%29.pdf)
- [Doracak i verifikimit për dezinformimin dhe manipulimin mediatik](https://datajournalism.com/read/handbook/verification-3)
- [Një udhëzues për të njohur pretendimet e rreme para se t’i demaskoni ato (prebunking): Një mënyrë premtuese për t’u mbrojtur nga dezinformatat](https://firstdraftnews.org/articles/a-guide-to-prebunking-a-promising-way-to-inoculate-against-misinformation/)
- [OverZero: Komunikimi gjatë kohërave të diskutueshme: Çfarë duhet të bëni dhe të mos bëni që të ngriheni mbi zhurmën](https://overzero.ghost.io/communicating-during-contentious-times-dos-and-donts-to-rise-above-the-noise/)
- [Hartëzimi i aktorëve vizualë](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html)
- [Paketa e veglave të dezinformimit](https://www.interaction.org/documents/disinformation-toolkit/)
- [Libri i punës për dobësimin e urrejtjes](https://www.ushmm.org/m/pdfs/20160229-Defusing-Hate-Workbook-3.pdf)
