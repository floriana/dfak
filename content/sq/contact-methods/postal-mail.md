---
layout: page
title: Posta postare
author: mfc
language: en
summary: Metodat e kontaktit
date: 2018-09
permalink: /en/contact-methods/postal-mail.md
parent: /en/
published: true
---

Dërgimi i postës është një metodë e ngadaltë e komunikimit nëse jeni duke u përballur me një situatë urgjente. Në varësi të juridiksioneve ku udhëton posta, autoritetet mund ta hapin postën dhe shpesh gjurmojnë dërguesin, vendndodhjen e dërgimit, marrësin dhe destinacionin.